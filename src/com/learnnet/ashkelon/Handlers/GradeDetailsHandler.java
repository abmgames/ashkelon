package com.learnnet.ashkelon.Handlers;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.learnnet.ashkelon.Objects.GradeDetailsData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class GradeDetailsHandler extends DefaultHandler{

	List<GradeDetailsData> messages;
	private GradeDetailsData currentMessage;
	
	
	private StringBuilder builder;
	
	public List<GradeDetailsData> getMessages(){
        return this.messages;
    }
	
	@Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        super.characters(ch, start, length);
        builder.append(ch, start, length);
    }
	
	@Override
    public void endElement(String uri, String localName, String name)
            throws SAXException {
        super.endElement(uri, localName, name);
        
            if (localName.equalsIgnoreCase("ASSIGNMENT_NAME"))
            {
            	currentMessage.name=builder.toString();
            }
            else if (localName.equalsIgnoreCase("ASSIGNMENT_SEMESTER"))
            {
            	currentMessage.semester=builder.toString();
            }
            else if (localName.equalsIgnoreCase("ASSIGNMENT_WEIGHT"))
            {
            	currentMessage.weight=builder.toString();
            }
            else if (localName.equalsIgnoreCase("GRADE"))
            {
            	currentMessage.grade=builder.toString();
            }
            else if (localName.equalsIgnoreCase("RECORD"))
            {
            	messages.add(currentMessage);
            }
            builder.setLength(0);    
        
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        messages = new ArrayList<GradeDetailsData>();
        builder = new StringBuilder();
    }

    @Override
    public void startElement(String uri, String localName, String name,
            Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (localName.equalsIgnoreCase("RECORD")){
            this.currentMessage = new GradeDetailsData();
        }
    }
	
}
