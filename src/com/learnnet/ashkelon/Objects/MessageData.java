package com.learnnet.ashkelon.Objects;

import java.util.Date;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class MessageData implements Comparable<Object>{
	
	private String MSG_TITLE = "";
	private String MSG_DTUPD = "";
	private String MSG_HTML  = "";
	
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		try {
			String[] temp1 = this.MSG_DTUPD.split(" |\\:|\\/"); //[day , month , year, hour , minutes , seconds]
			String[] temp2 = ((MessageData) arg0).MSG_DTUPD.split(" |\\:|\\/"); //[day , month , year, hour , minutes , seconds]

			// compare the years
			if (Integer.parseInt(temp1[2]) != Integer.parseInt(temp2[2]))
				return Integer.parseInt(temp2[2]) - Integer.parseInt(temp1[2]);
			// compare the months
			if (Integer.parseInt(temp1[1]) != Integer.parseInt(temp2[1]))
				return Integer.parseInt(temp2[1]) - Integer.parseInt(temp1[1]);
			// compare the days
			if (Integer.parseInt(temp1[0]) != Integer.parseInt(temp2[0]))
				return Integer.parseInt(temp2[0]) - Integer.parseInt(temp1[0]);
			// compare the hours
			if (Integer.parseInt(temp1[3]) != Integer.parseInt(temp2[3]))
				return Integer.parseInt(temp2[3]) - Integer.parseInt(temp1[3]);
			// compare the minutes
			if (Integer.parseInt(temp1[4]) != Integer.parseInt(temp2[4]))
				return Integer.parseInt(temp2[4]) - Integer.parseInt(temp1[4]);
			// compare the seconds
			if (Integer.parseInt(temp1[5]) != Integer.parseInt(temp2[5]))
				return Integer.parseInt(temp2[5]) - Integer.parseInt(temp1[5]);



		} catch (Exception ex) {

			return 0;
		}
		return 0;
	}
	
	public void setTITLE(String TITLE)
	{
		this.MSG_TITLE = TITLE;
	}
	
	public void setDTUPD(String DTUPD)
	{
		this.MSG_DTUPD = DTUPD;
	}
	
	public void setHTML(String HTML)
	{
		this.MSG_HTML = HTML;
	}
	
	public String getTITLE()
	{
		return MSG_TITLE;
	}
	
	public String getDTUPD()
	{
		return MSG_DTUPD;
	}
	
	public String getHTML()
	{
		return MSG_HTML;
	}

	

}
