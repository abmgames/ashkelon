package com.learnnet.ashkelon.Fragments;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Toast;

import com.learnnet.ashkelon.Interface.MyInterface;
import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Utils.GlobalDefs;
import com.learnnet.ashkelon.activity.ListPresenceActivity;
import com.learnnet.ashkelon.activity.Schedule;
import com.learnnet.ashkelon.activity.TeacherLessons;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TeacherLessonFragment extends Fragment implements GlobalDefs, MyInterface {

    // =================================================
    // FIELDS
    // =================================================

    private ExpandableListView mScheduleExpandableListView;
    private MyApplication mApp;
    private ExpandableListAdapter adapterExpandable;
    private Button btn;

    // =================================================
    // CONSTRUCTORS / SINGLETON
    // =================================================

    // =================================================
    // OVERRIDDEN METHODS
    // =================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_schedule, container, false);

        mScheduleExpandableListView = (ExpandableListView) v.findViewById(R.id.lv_schedule);

        mApp = ((TeacherLessons)getActivity()).getMyApp();

        setList();

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btn = (Button) view.findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                openDatePicker();
            }
        });
    }

    @Override
    public boolean isNetworkConnected() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String getPropertiesString(String key) {
        return null;
    }

    @Override
    public int getPropertiesInt(String key) {
        if (mApp.mPropertiesSpecific.containsKey(key)) {
            String tempString;
            tempString = (String) mApp.mPropertiesSpecific.get(key);
            tempString = tempString.replaceAll("\\s","");
            if (tempString == "" || tempString == null) {
                return 0;
            }
            else {
                return Integer.valueOf(tempString);
            }
        }
        else {
            return 0;
        }
    }

    @Override
    public boolean getPropertiesBoolean(String key) {
        return false;
    }

    @Override
    public Map<String, Object> getPropertiesHashTable(String key) {
        return null;
    }

    // =================================================
    // STATIC VARIABLES AND METHODS
    // =================================================

    // =================================================
    // CLASS LOGIC
    // =================================================

    private void setList() {

        ArrayList<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
        groupData = ((TeacherLessons)getActivity()).getGroupData();
        final ArrayList<List<Map<String, String>>> childrenData = ((TeacherLessons)getActivity()).getChildrenData();

        adapterExpandable = new SimpleExpandableListAdapter((TeacherLessons)getActivity(), groupData,
                R.layout.expandable_list_item, new String[] { "category" },
                new int[] { R.id.header }, childrenData, R.layout.teacher_lesson_item,
                new String[] { "event_time", "event_name", "event_location"}, new int[] { R.id.event_time,
                R.id.event_name, R.id.event_location});

        mScheduleExpandableListView.setAdapter(adapterExpandable);
        for (int i=0 ; i<groupData.size() ; i++) {
            mScheduleExpandableListView.expandGroup(i);
        }

        mScheduleExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long l) {

                if(((TeacherLessons)getActivity()).isFutureEvent(groupPosition))
                {
                    Toast.makeText(getActivity(), getString(R.string.future_event_error), Toast.LENGTH_SHORT).show();
                    return false;
                }

                Map<String, String> eventDetails = childrenData.get(groupPosition).get(childPosition);

                Intent presenceListIntent = new Intent(getActivity(), ListPresenceActivity.class);
                presenceListIntent.putExtra(ListPresenceActivity.MEETING_ID_KEY, eventDetails.get("event_id"));

                startActivity(presenceListIntent);

                return false;
            }
        });
    }

    private void openDatePicker() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog.OnDateSetListener odsl = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
                if (MyApplication.isInternetAvailable) {
                    ((TeacherLessons)getActivity()).setDate(arg1, arg2, arg3);
                }
                else {
                    mApp.showNoInternetDialog((TeacherLessons)getActivity());
                }
            }
        };

        DatePickerDialog dpdFromDate = new DatePickerDialog(getActivity(), odsl, mYear, mMonth,
                mDay);
        dpdFromDate.show();

        dpdFromDate.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    //et_to_date.setText("");
                }
            }
        });
    }

    // =================================================
    // GETTERS AND SETTERS
    // =================================================

    // =================================================
    // INNER CLASSES
    // =================================================

}