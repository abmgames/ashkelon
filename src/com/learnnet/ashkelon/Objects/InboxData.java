package com.learnnet.ashkelon.Objects;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class InboxData {

	public String title;
	public String body;
	public String date;
	public String id;
	public Boolean isSelected;
	
	public InboxData(String msgId, String title, String body, String date) {
		this.id =  msgId;
		this.title = title;
		this.body = body;
		this.date = date;
		this.isSelected = false;
	}
	
	public void setSelected(Boolean isSelected) {
		this.isSelected = isSelected;
	}
}
