package com.learnnet.ashkelon.Handlers;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.learnnet.ashkelon.Objects.YearsData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class MYearsHandler extends DefaultHandler {

	// =================================================
	// FIELDS
	// =================================================
	
	public List<YearsData> years;
	private YearsData currentYear;
	private StringBuilder builder;
	
	// =================================================
	// CONSTRUCTORS / SINGLETON
	// =================================================

	// =================================================
	// OVERRIDDEN METHODS
	// =================================================
	
	public List<YearsData> getMessages() {
        return this.years;
    }
	
	@Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        super.characters(ch, start, length);
        builder.append(ch, start, length);
    }
	
	@Override
    public void endElement(String uri, String localName, String name)
            throws SAXException {
        super.endElement(uri, localName, name);
        
            if (localName.equalsIgnoreCase("GRADE_YEAR"))
            {
            	currentYear.setYear(builder.toString());
            }
            else if (localName.equalsIgnoreCase("RECORD"))
            {
            	years.add(currentYear);
            }
            builder.setLength(0);    
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        years = new ArrayList<YearsData>();
        builder = new StringBuilder();
    }

    @Override
    public void startElement(String uri, String localName, String name,
            Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (localName.equalsIgnoreCase("RECORD")){
            this.currentYear = new YearsData();
        }
    }
	
	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	

	// =================================================
	// CLASS LOGIC
	// =================================================

	// =================================================
	// GETTERS AND SETTERS
	// =================================================

	// =================================================
	// INNER CLASSES
	// =================================================

}
