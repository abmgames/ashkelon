package com.learnnet.ashkelon.Handlers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class OutputDataHandler extends DefaultHandler {

	private String result;
	private String OutputData;
	private String currentMessage;
	private StringBuilder builder;
	
	public String getMessages(){
		if(this.result.equalsIgnoreCase("success"))
		 {
			 return this.OutputData;
		 }
		 else
		 {
			 return "error data";
		 }	
	}
	
	@Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        super.characters(ch, start, length);
        builder.append(ch, start, length);
    }
	
	@Override
    public void endElement(String uri, String localName, String name)
            throws SAXException {
        super.endElement(uri, localName, name);
        if (this.currentMessage != null){
            if (localName.equalsIgnoreCase("ProcessRequestResult")){
                currentMessage = builder.toString();
            } else if (localName.equalsIgnoreCase("Result")){
            	result = builder.toString();
            }/* else if (localName.equalsIgnoreCase(DESCRIPTION)){
                currentMessage.setDescription(builder.toString());
            } else if (localName.equalsIgnoreCase(PUB_DATE)){
                currentMessage.setDate(builder.toString());
            }*/ else if (localName.equalsIgnoreCase("OutputData")){
            	OutputData = builder.toString();
            }
            builder.setLength(0);    
        }
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        builder = new StringBuilder();
    }

    @Override
    public void startElement(String uri, String localName, String name,
            Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (localName.equalsIgnoreCase("ProcessRequestResult")){
            this.currentMessage = new String();
        }
    }
}
