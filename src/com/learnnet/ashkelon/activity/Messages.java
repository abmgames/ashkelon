package com.learnnet.ashkelon.activity;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Communication.MyMichlolRequests;
import com.learnnet.ashkelon.Fragments.MessagesExpandFragment;
import com.learnnet.ashkelon.Fragments.MessagesFragment;
import com.learnnet.ashkelon.Interface.MyInterface;
import com.learnnet.ashkelon.Objects.MessageData;
import com.learnnet.ashkelon.Utils.GlobalDefs;
import com.learnnet.ashkelon.Utils.xmlParser;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class Messages extends ActionBarActivity implements GlobalDefs, MyInterface, OnItemSelectedListener {

	// =================================================
	// FIELDS
	// =================================================

	private MyApplication mApp;
	private int mCurrentFragmentIndex;
	private ActionBar mActionBar;
	private ProgressDialog spinnerDialog;
	private String msgHtml;
	private List<MessageData> mData;
	private TextView actionBarTitle;

	// =================================================
	// OVERRIDDEN METHODS
	// =================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_messages);

		this.mApp = (MyApplication)getApplication();

		// Inflate your custom action bar layout
		final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.action_bar_centered_text, null);

		// Disable drawer swipe gesture 
		DrawerLayout mDrawerLayout;
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

		// Set up your ActionBar
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayShowCustomEnabled(true);
		mActionBar.setCustomView(actionBarLayout);

		// Enable ActionBar app icon to behave as action to toggle nav drawer
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayUseLogoEnabled(false);
		mActionBar.setDisplayHomeAsUpEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);

		// Set ActionBar's text
		actionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
		actionBarTitle.setText(getResources().getString(R.string.my_messages_title));

		if (savedInstanceState == null) {
			//new GetMessagesTask().execute();

			displaySpinner();

			String username = mApp.mPrefs.getString(PREF_USER_NAME, "");
			String url = mApp.mPrefs.getString(PREF_MICHLOL_URL, "");

			MyMichlolRequests michlolMessagesRequest = new MyMichlolRequests();
			michlolMessagesRequest.call(
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						xmlParser xp = new xmlParser();
						mData = xp.parseMMessageData(response);

						Collections.sort(mData);

						hideSpinner();
						selectItem(0);
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						hideSpinner();
					}
				},
			REQ_MESSAGES, username, "UnsedValueJustForChecks", null, null, url, false); // false doesn't matter here.
		}

	}
	
	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_inner, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean isNetworkConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			return (String) mApp.mPropertiesSpecific.get(key);
		}
		else {
			return null;
		}
	}

	@Override
	public int getPropertiesInt(String key) {
		return 0;
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBackPressed() {
		switch (mCurrentFragmentIndex) {
		case 0:
			super.onBackPressed();
			break;
		case 1:
			selectItem(0);
			break;
		default:
			super.onBackPressed(); // Open default fragment (Messages menu)
			break;
		}
	}

	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================

	// =================================================
	// CLASS LOGIC
	// =================================================

	private void selectItem(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (position) {
		case 0:
			fragment = new MessagesFragment();
			actionBarTitle.setText(getResources().getString(R.string.my_messages_title));
			break;
		case 1:
			fragment = new MessagesExpandFragment();
			Bundle args = new Bundle();
			args.putString(BUNDLE_MESSAGE_HTML, msgHtml);
			fragment.setArguments(args);
			break;
		default:
			fragment = new MessagesFragment();
			break;
		}
		if (fragment != null) {

			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			mCurrentFragmentIndex = position;
		}
	}

	public void displaySpinner() {
		spinnerDialog = ProgressDialog.show(Messages.this, "",
				getString(R.string.my_wait), true);
	}

	public void hideSpinner() {
		spinnerDialog.cancel();
	}

	public void openMessage(MessageData msg) {
		msgHtml = msg.getHTML();
		actionBarTitle.setText(msg.getTITLE());
		selectItem(1);
	}

	/*private String michlolRequestMessages(String username, String url) {
		MyMichlolRequests michlolMessagesRequest = new MyMichlolRequests();
		return michlolMessagesRequest.call(REQ_MESSAGES, username, "UnsedValueJustForChecks",
				null, null, url, false); // false doesn't matter here.
	}*/

	// =================================================
	// GETTERS AND SETTERS
	// =================================================

	public MyApplication getMyApp() {
		return this.mApp;
	}

	public List<MessageData> getMessagesData() {
		return this.mData;
	}

}