package com.learnnet.ashkelon.Communication;

import android.util.ArrayMap;

import java.io.DataInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.Map;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.learnnet.ashkelon.MyApplication;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class MyIdmLoginRequest {

	public void newLogin(final String idmUrl, final String michlolUrl, Response.Listener<String> respListener, Response.ErrorListener errorListener, final String username, final String password)
	{
		StringRequest req = new StringRequest(Request.Method.POST,
				idmUrl,
				respListener,
				errorListener) {

			@Override
			public byte[] getBody() throws com.android.volley.AuthFailureError {
				return soapRequest(username, password).getBytes();
			}

			@Override
			public String getBodyContentType()
			{
				return "application/json; charset=utf-8";
			}

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				//Map<String, String> headers = super.getHeaders();

				Map<String, String> params = new HashMap<>();

				params.put("SOAPAction", michlolUrl);
				params.put("Content-Type", "text/xml; charset=utf-8");

				return params;
			}

		};

		MyApplication.getInstance().addToRequestQueue(req);
	}

	private String soapRequest(String user, String password) {
		final StringBuffer soap = new StringBuffer(); // Comment: 4
		soap.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
		soap.append("<soapenv:Envelope xmlns:ws=\"http://ws.edp\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n");
		soap.append("<soapenv:Header/>\n");
		soap.append("<soapenv:Body>\n");
		soap.append("<ws:authRequest>\n");
		soap.append("<uid>" + user + "</uid>\n");
		soap.append("<password>" + password + "</password>\n");
		soap.append("</ws:authRequest>\n");
		soap.append("</soapenv:Body>\n");
		soap.append("</soapenv:Envelope>\n");

		return soap.toString();

	}
	
}
