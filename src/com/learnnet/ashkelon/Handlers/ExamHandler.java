package com.learnnet.ashkelon.Handlers;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.learnnet.ashkelon.Objects.ExamData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class ExamHandler extends DefaultHandler{

	List<ExamData> messages;
	private ExamData currentMessage;
	
	
	private StringBuilder builder;
	
	public List<ExamData> getMessages(){
        return this.messages;
    }
	
	@Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        super.characters(ch, start, length);
        builder.append(ch, start, length);
    }
	
	@Override
    public void endElement(String uri, String localName, String name)
            throws SAXException {
        super.endElement(uri, localName, name);
        
            if (localName.equalsIgnoreCase("EXAM_SEMESTER"))
            {
            	currentMessage.EXAM_SEMESTER=builder.toString();
            }
            else if (localName.equalsIgnoreCase("EXAM_NUMBER"))
            {
            	currentMessage.EXAM_NUMBER=builder.toString();
            }
            else if (localName.equalsIgnoreCase("EXAM_DATE_INFO"))
            {
            	currentMessage.EXAM_DATE_INFO=builder.toString().replace("<", "").replace(">", "").replace("br", "");
            }
            else if (localName.equalsIgnoreCase("EXAM_TIME_INFO"))
            {
            	currentMessage.EXAM_TIME_INFO=builder.toString();
            }
            else if (localName.equalsIgnoreCase("EXAM_HEB_DATE"))
            {
            	currentMessage.EXAM_HEB_DATE=builder.toString().replace("<", "").replace(">", "").replace("br", "");
            }
            else if (localName.equalsIgnoreCase("COURSE_DESCRIPTION"))
            {
            	currentMessage.COURSE_DESCRIPTION=builder.toString().replace("<", "").replace(">", "").replace("br", "");
            }
            else if (localName.equalsIgnoreCase("TASK_DESCRIPTION"))
            {
            	currentMessage.TASK_DESCRIPTION=builder.toString();
            }
            else if (localName.equalsIgnoreCase("TEACHER_NAME"))
            {
            	currentMessage.TEACHER_NAME=builder.toString();
            }
            else if (localName.equalsIgnoreCase("ROOM_NAME"))
            {
            	currentMessage.ROOM_NAME=builder.toString();
            }
            else if (localName.equalsIgnoreCase("EXPOSURE_INFO"))
            {
            	currentMessage.EXPOSURE_INFO=builder.toString();
            }
            else if (localName.equalsIgnoreCase("EXPIRED"))
            {
            	currentMessage.EXPIRED=builder.toString();
            }
            else if (localName.equalsIgnoreCase("KRS_MIS_KVUZA"))
            {
            	currentMessage.KRS_MIS_KVUZA=builder.toString();
            }
            else if (localName.equalsIgnoreCase("SHLOHA"))
            {
            	currentMessage.SHLOHA=builder.toString();
            }
            else if (localName.equalsIgnoreCase("STATUS_DESCRIPTION"))
            {
            	currentMessage.STATUS_DESCRIPTION=builder.toString();
            }
            else if (localName.equalsIgnoreCase("RECORD"))
            {
            	messages.add(currentMessage);
            }
            builder.setLength(0);    
        
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        messages = new ArrayList<ExamData>();
        builder = new StringBuilder();
    }

    @Override
    public void startElement(String uri, String localName, String name,
            Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (localName.equalsIgnoreCase("RECORD")){
            this.currentMessage = new ExamData();
        }
    }
	
}
