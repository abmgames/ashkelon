package com.learnnet.ashkelon.Handlers;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.learnnet.ashkelon.Objects.GradeData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class MGradesHandler extends DefaultHandler{

	List<GradeData> messages;
	private GradeData currentMessage;
	
	
	private StringBuilder builder;
	
	public List<GradeData> getMessages(){
        return this.messages;
    }
	
	@Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        super.characters(ch, start, length);
        builder.append(ch, start, length);
    }
	
	@Override
    public void endElement(String uri, String localName, String name)
            throws SAXException {
        super.endElement(uri, localName, name);
        
            if (localName.equalsIgnoreCase("COURSE_NUMBER"))
            {
            	currentMessage.courseNumber=builder.toString();
            }
            else if (localName.equalsIgnoreCase("COURSE_ID"))
            {
            	currentMessage.courseId=builder.toString();
            }
            else if (localName.equalsIgnoreCase("SEMESTERA_GRADE"))
            {
            	currentMessage.semesterAGrade=builder.toString();
            }
            else if (localName.equalsIgnoreCase("SEMESTERB_GRADE"))
            {
            	currentMessage.semesterBGrade=builder.toString();
            }
            else if (localName.equalsIgnoreCase("SEMESTERKAIZ_GRADE"))
            {
            	currentMessage.semesterKaitzGrade=builder.toString();
            }
            else if (localName.equalsIgnoreCase("FINALGRADE"))
            {
            	currentMessage.finalGrade=builder.toString();
            }
            else if (localName.equalsIgnoreCase("COURSE_NAME"))
            {
            	currentMessage.courseName=builder.toString();
            }
            else if (localName.equalsIgnoreCase("CREDITS"))
            {
            	currentMessage.credits=builder.toString();
            }
            else if (localName.equalsIgnoreCase("ZIN_MILULI"))
            {
            	currentMessage.verbalGrade=builder.toString();
            }
            else if (localName.equalsIgnoreCase("RECORD"))
            {
            	messages.add(currentMessage);
            }
            builder.setLength(0);    
        
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        messages = new ArrayList<GradeData>();
        builder = new StringBuilder();
    }

    @Override
    public void startElement(String uri, String localName, String name,
            Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (localName.equalsIgnoreCase("RECORD")){
            this.currentMessage = new GradeData();
        }
    }
	
}
