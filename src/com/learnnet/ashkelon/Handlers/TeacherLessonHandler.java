package com.learnnet.ashkelon.Handlers;

import com.learnnet.ashkelon.Objects.TeacherLessonData;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class TeacherLessonHandler extends DefaultHandler {
    List<TeacherLessonData> messages;
    private TeacherLessonData currentMessage;


    private StringBuilder builder;

    public List<TeacherLessonData> getMessages(){
        return this.messages;
    }

    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        super.characters(ch, start, length);
        builder.append(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String name)
            throws SAXException {
        super.endElement(uri, localName, name);

        /*
        ENDTIME = "12:00";
    EVENTID = 1229657;
    LOCATION = "\U05d1\U05e0\U05d9\U05df28-\U05de\U05e2\U05d1\U05d3\U05d4 4";
    STARTTIME = "12:00";
    SUBJECT = "\U05d9\U05d9\U05e9\U05d5\U05de\U05d9 \U05de\U05d7\U05e9\U05d1 \U05d1\U05e0\U05d9\U05d4\U05d5\U05dc";
    THEDATE = "22/02/2016";
         */

        if (localName.equalsIgnoreCase("ENDTIME"))
        {
            currentMessage.ENDTIME = builder.toString();
        }
        else if (localName.equalsIgnoreCase("EVENTID"))
        {
            currentMessage.EVENTID = builder.toString();
        }
        else if (localName.equalsIgnoreCase("LOCATION"))
        {
            currentMessage.LOCATION = builder.toString();
        }
        else if (localName.equalsIgnoreCase("STARTTIME"))
        {
            currentMessage.STARTTIME = builder.toString();
        }
        else if (localName.equalsIgnoreCase("SUBJECT"))
        {
            currentMessage.SUBJECT = builder.toString();
        }
        else if (localName.equalsIgnoreCase("THEDATE"))
        {
            currentMessage.THEDATE = builder.toString().replace(" 00:00:00", "");
        }
        else if (localName.equalsIgnoreCase("RECORD"))
        {
            messages.add(currentMessage);
        }

        builder.setLength(0);

    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        messages = new ArrayList<TeacherLessonData>();
        builder = new StringBuilder();
    }

    @Override
    public void startElement(String uri, String localName, String name,
                             Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (localName.equalsIgnoreCase("RECORD")){
            this.currentMessage = new TeacherLessonData();
        }
    }
}