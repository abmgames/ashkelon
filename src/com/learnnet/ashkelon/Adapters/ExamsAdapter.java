package com.learnnet.ashkelon.Adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Objects.ExamData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class ExamsAdapter extends ArrayAdapter<ExamData> {

	public Activity activity;
	public List<ExamData> tweets;

	public ExamsAdapter(Activity a, int textViewResourceId, List<ExamData> tweets) {
		super(a, textViewResourceId, tweets);
		this.tweets = tweets;
		activity = a;
	}

	public static class ViewHolder {
		public TextView text1;
		public TextView text2;
		public TextView text3;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;
		ViewHolder holder;

		final ExamData tweet = tweets.get(position);

		if (v == null) {

			LayoutInflater vi = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.exam_list_item, null);
			holder = new ViewHolder();
			holder.text1 = (TextView) v.findViewById(R.id.line1);
			holder.text2 = (TextView) v.findViewById(R.id.line2);
			holder.text3 = (TextView) v.findViewById(R.id.line3);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		if (tweet != null) {

			String date = tweet.EXAM_DATE_INFO.replace(tweet.EXAM_HEB_DATE, "")
					.substring(2).trim();
			
			holder.text1.setText(date);
			holder.text2.setText(tweet.COURSE_DESCRIPTION.replaceAll("\\d+", "").replace("-", ""));
			holder.text3.setText(tweet.STATUS_DESCRIPTION);
		}

		return v;

	}

}
