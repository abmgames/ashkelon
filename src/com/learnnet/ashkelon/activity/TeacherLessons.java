package com.learnnet.ashkelon.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.GoogleAnalytics;
/*import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;*/
import com.google.android.gms.common.api.GoogleApiClient;
import com.learnnet.ashkelon.Communication.MyMichlolRequests;
import com.learnnet.ashkelon.Fragments.TeacherLessonFragment;
import com.learnnet.ashkelon.Interface.MyInterface;
import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.Objects.ScheduleData;
import com.learnnet.ashkelon.Objects.TeacherLessonData;
import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Utils.GlobalDefs;
import com.learnnet.ashkelon.Utils.xmlParser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TeacherLessons extends ActionBarActivity implements GlobalDefs, MyInterface {

    // =================================================
    // FIELDS
    // =================================================

    private MyApplication mApp;
    private int mCurrentFragmentIndex;
    private ActionBar mActionBar;
    private ProgressDialog spinnerDialog;
    private TextView actionBarTitle;
    private String mStartDate;
    private String mEndDate;
    private Date dateAll;
    private List<TeacherLessonData> mData = new ArrayList<TeacherLessonData>();
    private String[] category = new String[6];
    private ArrayList<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
    private ArrayList<String> subjectList = new ArrayList<String>();
    private ArrayList<List<Map<String, String>>> childrenData = new ArrayList<List<Map<String, String>>>();


    private Date[] days;
    private Date currentTime;
    private boolean spinnerShown = false;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    // =================================================
    // OVERRIDDEN METHODS
    // =================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        this.mApp = (MyApplication) getApplication();

        // Inflate your custom action bar layout
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_centered_text, null);

        // Disable drawer swipe gesture
        DrawerLayout mDrawerLayout;
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        // Set up your ActionBar
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setCustomView(actionBarLayout);

        // Enable ActionBar app icon to behave as action to toggle nav drawer
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayUseLogoEnabled(false);
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);

        // Set ActionBar's text
        actionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
        actionBarTitle.setText(getResources().getString(R.string.my_schedule_title));

        if (savedInstanceState == null) {
            // TODO : [A+] init start and end dates. !!
            dateAll = new Date();
            getScheduleTBL(dateAll);
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        currentTime = Calendar.getInstance().getTime();
    }

    @Override
    public boolean isNetworkConnected() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String getPropertiesString(String key) {
        if (mApp.mPropertiesSpecific.containsKey(key)) {
            return (String) mApp.mPropertiesSpecific.get(key);
        } else {
            return null;
        }
    }

    @Override
    public int getPropertiesInt(String key) {
        return 0;
    }

    @Override
    public boolean getPropertiesBoolean(String key) {
        return false;
    }

    @Override
    public Map<String, Object> getPropertiesHashTable(String key) {
        return null;
    }

    @Override
    public void onBackPressed() {
        switch (mCurrentFragmentIndex) {
            case 0:
                super.onBackPressed();
                break;
            case 1:
                selectItem(0);
                break;
            default:
                super.onBackPressed(); // Open default fragment (Schedule menu)
                break;
        }
    }

    // =================================================
    // STATIC VARIABLES AND METHODS
    // =================================================

    // =================================================
    // CLASS LOGIC
    // =================================================

    private void selectItem(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new TeacherLessonFragment();
                actionBarTitle.setText(getResources().getString(R.string.my_schedule_title));
                break;
            default:
                fragment = new TeacherLessonFragment();
                break;
        }
        if (fragment != null) {

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            mCurrentFragmentIndex = position;
        }
    }

    public void displaySpinner() {

        if (!spinnerShown) {

            try {
                spinnerDialog = ProgressDialog.show(
                        TeacherLessons.this,
                        "",
                        getString(R.string.my_wait),
                        true,
                        true,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                try {
                                    //mission.cancel(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                finish();
                                spinnerShown = false;
                            }
                        }
                );
            } catch (Exception e) {
                e.printStackTrace();
            }

            spinnerShown = true;
        }
    }

    public void hideSpinner() {
        try {
            spinnerDialog.hide();
            spinnerDialog = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        spinnerShown = false;
    }

    /*private String michlolRequestSchedule(String username, String url,
                                          String startDate, String endDate) {
        MyMichlolRequests michlolMessagesRequest = new MyMichlolRequests();

        return michlolMessagesRequest.call(REQ_LECTURER_SCHEDULE, username, "UnsedValueJustForChecks",
                startDate, endDate, url, false); // false doesn't matter here.
    }*/

    @SuppressLint("SimpleDateFormat")
    public void getScheduleTBL(final Date refDate) {

        mData.clear();

        category = new String[6];

        groupData.clear();
        subjectList.clear();
        childrenData.clear();

        days = getDaysOfWeek(refDate, Calendar.SUNDAY);

        category[0] = getString(R.string.my_day_1) + " "
                + DateFormat.format("dd/MM/yyyy", days[0]);
        category[1] = getString(R.string.my_day_2) + " "
                + DateFormat.format("dd/MM/yyyy", days[1]);
        category[2] = getString(R.string.my_day_3) + " "
                + DateFormat.format("dd/MM/yyyy", days[2]);
        category[3] = getString(R.string.my_day_4) + " "
                + DateFormat.format("dd/MM/yyyy", days[3]);
        category[4] = getString(R.string.my_day_5) + " "
                + DateFormat.format("dd/MM/yyyy", days[4]);
        category[5] = getString(R.string.my_day_6) + " "
                + DateFormat.format("dd/MM/yyyy", days[5]);

        buildGroupData();

        SimpleDateFormat postFormater = new SimpleDateFormat(
                "dd/MM/yyyy");

        mStartDate = postFormater.format(days[0]);
        mEndDate = postFormater.format(days[6]);

        if (!spinnerShown) {
            /*mission = new GetScheduleTask();

            try {
                mission.execute(mStartDate, mEndDate);
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            String username = mApp.mPrefs.getString(PREF_USER_NAME, "");
            String url = mApp.mPrefs.getString(PREF_MICHLOL_URL, "");

            MyMichlolRequests michlolMessagesRequest = new MyMichlolRequests();

            michlolMessagesRequest.call(
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        xmlParser xp = new xmlParser();
                        mData = xp.parseTeacherLessonsData(response);

                        //Collections.sort(mData);
                        buildChildrenData();
                        hideSpinner();
                        selectItem(0);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideSpinner();
                    }
                },
            REQ_LECTURER_SCHEDULE, username, "UnsedValueJustForChecks", mStartDate, mEndDate, url, false); // false doesn't matter here.
        }
    }

    private static Date[] getDaysOfWeek(Date refDate, int firstDayOfWeek) {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.setTime(refDate);
        calendar.set(Calendar.DAY_OF_WEEK, firstDayOfWeek);
        Date[] daysOfWeek = new Date[7];
        for (int i = 0; i < 7; i++) {
            daysOfWeek[i] = calendar.getTime();
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        return daysOfWeek;
    }

    private void buildGroupData() {
        for (String subjectEntity : category) {
            Map<String, String> groupMap = new HashMap<String, String>();
            groupData.add(groupMap);
            groupMap.put("category", subjectEntity);
            subjectList.add(subjectEntity);
        }
    }

    private void buildChildrenData() {
        ArrayList<Map<String, String>> children1 = new ArrayList<Map<String, String>>();
        ArrayList<Map<String, String>> children2 = new ArrayList<Map<String, String>>();
        ArrayList<Map<String, String>> children3 = new ArrayList<Map<String, String>>();
        ArrayList<Map<String, String>> children4 = new ArrayList<Map<String, String>>();
        ArrayList<Map<String, String>> children5 = new ArrayList<Map<String, String>>();
        ArrayList<Map<String, String>> children6 = new ArrayList<Map<String, String>>();

        Date[] days = getDaysOfWeek(dateAll, Calendar.SUNDAY);

        for (int index = 0; index < mData.size(); index++) {
            Map<String, String> childMap = new HashMap<String, String>();

            String date = "12:00 - 12:00";

            try {
                date = mData.get(index).STARTTIME.substring(
                        mData.get(index).STARTTIME.indexOf(" ") + 1,
                        mData.get(index).STARTTIME.length() - 3)
                        + " - "
                        + mData.get(index).ENDTIME.substring(
                        mData.get(index).ENDTIME.indexOf(" ") + 1,
                        mData.get(index).ENDTIME.length() - 3);
            } catch (Exception e) {
                // TODO: handle exception
            }
//"event_time", "event_name", "event_location"
            childMap.put("event_time", date);
            childMap.put("event_name", mData.get(index).SUBJECT);
            childMap.put("event_location", mData.get(index).LOCATION);
            childMap.put("event_id", mData.get(index).EVENTID);
            childMap.put("index", String.valueOf(index));

            if (mData.get(index).THEDATE.equals(DateFormat.format(
                    "dd/MM/yyyy", days[0]))) {
                children1.add(childMap);
            } else if (mData.get(index).THEDATE.equals(DateFormat
                    .format("dd/MM/yyyy", days[1]))) {
                children2.add(childMap);
            } else if (mData.get(index).THEDATE.equals(DateFormat
                    .format("dd/MM/yyyy", days[2]))) {
                children3.add(childMap);
            } else if (mData.get(index).THEDATE.equals(DateFormat
                    .format("dd/MM/yyyy", days[3]))) {
                children4.add(childMap);
            } else if (mData.get(index).THEDATE.equals(DateFormat
                    .format("dd/MM/yyyy", days[4]))) {
                children5.add(childMap);
            } else if (mData.get(index).THEDATE.equals(DateFormat
                    .format("dd/MM/yyyy", days[5]))) {
                children6.add(childMap);
            }

        }
        childrenData.add(children1);
        childrenData.add(children2);
        childrenData.add(children3);
        childrenData.add(children4);
        childrenData.add(children5);
        childrenData.add(children6);
    }

    @SuppressWarnings("deprecation")
    public void setDate(int year, int month, int day) {
        dateAll.setDate(day);
        dateAll.setMonth(month);
        dateAll.setYear(year - 1900);
        getScheduleTBL(dateAll);
    }

    // =================================================
    // GETTERS AND SETTERS
    // =================================================

    public MyApplication getMyApp() {
        return this.mApp;
    }

    public List<TeacherLessonData> getScheduleData() {
        return this.mData;
    }

    public ArrayList<List<Map<String, String>>> getChildrenData() {
        return this.childrenData;
    }

    public boolean isFutureEvent(int dayIndex){
        try{
            return days[dayIndex].after(currentTime);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return true;
    }

    public ArrayList<Map<String, String>> getGroupData() {
        return this.groupData;
    }


}
