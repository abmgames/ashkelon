package com.learnnet.ashkelon.Objects;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class TeacherExamData {
	
	public String COURSENUMBER = "";
	public String COURSENAME = "";
	public String FIRSTDATE = "";
	public String FIRSTTIME = "";
	public String SECONDDATE = "";
	public String SECONDTIME = "";
	public String THIRDDATE = "";
	public String THIRDTIME = "";
	
}
