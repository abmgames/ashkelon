package com.learnnet.ashkelon;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

public class PushAlertActivity extends Activity {

	String message = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.push_popup_dialog);
		
		Bundle extras = getIntent().getExtras();

		if (extras != null) {

			message = extras.getString("Message");
		}

		try {
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setIcon(R.drawable.ic_launcher);
			alertDialog.setTitle(getString(R.string.app_name));
			alertDialog.setCancelable(false);
			
			alertDialog.setMessage(message);
		
			alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
					getString(R.string.my_ok),
					new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int id) {

							// Code from UrbanAirShip - Not working at the moment.
//							Intent launchIntent = new Intent();
//							launchIntent.setComponent(new ComponentName("com.learnnet.ashkelon",
//									"com.learnnet.ashkelon.activity.Splash"));
//							launchIntent.setAction(Intent.ACTION_MAIN);
//							launchIntent.addCategory(Intent.CATEGORY_LAUNCHER);
//							launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//									| Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
//							startActivity(launchIntent);
							
							Intent intent = new Intent("android.intent.action.MAIN");
						    intent.setComponent(ComponentName.unflattenFromString("com.learnnet.ashkelon/com.learnnet.ashkelon.activity.Splash"));
						    intent.addCategory("android.intent.category.LAUNCHER");
						    startActivity(intent);
							
							finish();
							
							dialog.cancel();

						}
					});

			alertDialog.show();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}