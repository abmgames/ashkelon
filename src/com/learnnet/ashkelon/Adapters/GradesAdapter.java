package com.learnnet.ashkelon.Adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Objects.GradeData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class GradesAdapter extends ArrayAdapter<GradeData> {

	public Activity activity;
	public List<GradeData> tweets;
	private MyApplication mApp;

	public GradesAdapter(Activity a, int textViewResourceId, List<GradeData> tweets) {
		super(a, textViewResourceId, tweets);
		this.tweets = tweets;
		activity = a;
		mApp = (MyApplication) activity.getApplication();
	}

	public static class ViewHolder {
		public TextView text1;
		public TextView text2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;
		ViewHolder holder;

		final GradeData tweet = tweets.get(position);

		if (v == null) {

			LayoutInflater vi = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.grade_list_item, null);
			holder = new ViewHolder();
			holder.text1 = (TextView) v.findViewById(R.id.line1);
			holder.text2 = (TextView) v.findViewById(R.id.line2);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		if (tweet != null) {

//			holder.text1.setText(tweet.DTUPDZIN.substring(0, tweet.DTUPDZIN.indexOf(" ")));
			holder.text1.setText(tweet.courseName);
			if (tweet.finalGrade != null) {
				if (!TextUtils.isEmpty(tweet.finalGrade)) {
					//getPropertiesString("400")
					if (Integer.parseInt(tweet.finalGrade) > 100) {
						//holder.text2.setText("השתתף");
						String statusCode = getPropertiesString(tweet.finalGrade);

						if(TextUtils.isEmpty(statusCode))
							holder.text2.setText(tweet.finalGrade);
						else
							holder.text2.setText(statusCode);
					}
					else {
						holder.text2.setText(tweet.finalGrade);
					}
				}
				else {
					holder.text2.setText("לא קיים מידע");
				}
			}
			else {
				holder.text2.setText("לא קיים מידע");
			}
		}

		return v;

	}

	public String getPropertiesString(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			return (String) mApp.mPropertiesSpecific.get(key);
		}
		else {
			return null;
		}
	}

}
