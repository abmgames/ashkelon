package com.learnnet.ashkelon.Adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Utils.GlobalDefs;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class MyAcademicMenuListViewAdapter extends BaseAdapter implements GlobalDefs {

	@SuppressWarnings("unused")
	private Activity activity;
    private ArrayList<Map<String, String>> data;
    private LayoutInflater inflater = null;
    
    public MyAcademicMenuListViewAdapter(Activity activity, ArrayList<Map<String, String>> data) {
    	this.activity = activity;
    	this.data = data;
    	this.inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.academic_menu_list_item, null);
 
        TextView title = (TextView)vi.findViewById(R.id.tvItemTitle); // title
        TextView desc = (TextView)vi.findViewById(R.id.tvItemDesc); // description
        ImageView image = (ImageView)vi.findViewById(R.id.ivItemImage); // thumb image
 
        Map<String, String> itemMap = new HashMap<String, String>();
        itemMap = data.get(position);
 
        // Setting all values in listview
        Context context = image.getContext();
        int id = context.getResources().getIdentifier(itemMap.get(MENU_IMG), "drawable", context.getPackageName());
        title.setText(itemMap.get(MENU_TITLE));
        desc.setText(itemMap.get(MENU_DESC));
        image.setImageResource(id);
        return vi;
	}

}
