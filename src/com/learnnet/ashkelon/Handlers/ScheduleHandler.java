package com.learnnet.ashkelon.Handlers;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.learnnet.ashkelon.Objects.ScheduleData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class ScheduleHandler extends DefaultHandler{

	List<ScheduleData> messages;
	private ScheduleData currentMessage;
	
	
	private StringBuilder builder;
	
	public List<ScheduleData> getMessages(){
        return this.messages;
    }
	
	@Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        super.characters(ch, start, length);
        builder.append(ch, start, length);
    }
	
	@Override
    public void endElement(String uri, String localName, String name)
            throws SAXException {
        super.endElement(uri, localName, name);
        
            if (localName.equalsIgnoreCase("BEGINTIME"))
            {
            	currentMessage.BEGINTIME=builder.toString();
            }
            else if (localName.equalsIgnoreCase("ENDTIME"))
            {
            	currentMessage.ENDTIME=builder.toString();
            }
            else if (localName.equalsIgnoreCase("LECTURERNAME"))
            {
            	currentMessage.LECTURERNAME=builder.toString();
            }
            else if (localName.equalsIgnoreCase("RESOURCEDATE"))
            {
            	currentMessage.RESOURCEDATE=builder.toString();
            }
            else if (localName.equalsIgnoreCase("ROOMNAME"))
            {
            	currentMessage.ROOMNAME=builder.toString();
            }
            else if (localName.equalsIgnoreCase("SUBJECT"))
            {
            	currentMessage.SUBJECT=builder.toString();
            }
            else if (localName.equalsIgnoreCase("RECORD"))
            {
            	messages.add(currentMessage);
            }
            builder.setLength(0);    
        
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        messages = new ArrayList<ScheduleData>();
        builder = new StringBuilder();
    }

    @Override
    public void startElement(String uri, String localName, String name,
            Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (localName.equalsIgnoreCase("RECORD")){
            this.currentMessage = new ScheduleData();
        }
    }
	
}
