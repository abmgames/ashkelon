package com.learnnet.ashkelon;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.learnnet.ashkelon.Interface.MyInterface;
import com.learnnet.ashkelon.Objects.IdmLoginData;
import com.learnnet.ashkelon.Objects.UserDetailsData;
import com.learnnet.ashkelon.Objects.YearsData;
import com.learnnet.ashkelon.Utils.GlobalDefs;
import com.onesignal.OneSignal;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class MyApplication extends Application implements GlobalDefs, MyInterface {

	// =================================================
	// FIELDS
	// =================================================
	
	public Map<String, Map<String, Object>> mProperties;
	public Map<String, Object> mPropertiesSpecific;
	public SharedPreferences mPrefs;
	public UserDetailsData mCurrentUserDetails;
	public List<YearsData> mCurrentUserYears;
	public IdmLoginData mIdmLoginData;
	public static boolean isInternetAvailable;

	//API
	public static String[] Kxx;
	public static String TOKEN;
	public static String pU, pP;

	//Debug - Do not forget to disable debug mode before you publish a new version!!
	public static final boolean DEBUG_MODE = false;
	
	public HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();
	public static MyApplication instance;
	
	// =================================================
	// CONSTRUCTORS / SINGLETON
	// =================================================
	
	// =================================================
	// OVERRIDDEN METHODS
	// =================================================

	private RequestQueue mRequestQueue;

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}

		return mRequestQueue;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();
		OneSignal.startInit(this).init();

		/*PushManager.init(getApplicationContext(), "710381322654", "eddd5b57-b4f0-47d6-80f0-d0cd85c237de");
		PushManager.getInstance(getApplicationContext()).setShouldStackNotifications(false);
		PushManager.getInstance(getApplicationContext()).setIntentNameToLaunch("com.learnnet.ashkelon.PushAlertActivity");*/
		mPrefs = getSharedPreferences(PREF_FILE_NAME, Activity.MODE_PRIVATE);
		
		getTracker(TrackerName.APP_TRACKER);

		instance = this;

	}

	public static MyApplication getInstance(){
		return instance;
	}

	@Override
	public boolean isNetworkConnected() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null) {
			// There are no active networks.
			return false;
		} else
			return true;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		return 0;
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}

	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================

	// The following line should be changed to include the correct property id.
	private static final String PROPERTY_ID = "UA-52666410-12";

	public enum TrackerName {
		APP_TRACKER, // Tracker used only in this app.
		GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
	}

	// =================================================
	// CLASS LOGIC
	// =================================================

	public synchronized Tracker getTracker(TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {

			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			Tracker t;
			
			if (trackerId == TrackerName.APP_TRACKER) {
				t = analytics.newTracker(R.xml.app_tracker);
			}
			else {
				t = analytics.newTracker(PROPERTY_ID);
			}
			mTrackers.put(trackerId, t);
		}
		return mTrackers.get(trackerId);
	}
	
	public void sendAnalyticScreen(String screenName) {
		
		Tracker t = getTracker(TrackerName.APP_TRACKER);
		t.setScreenName(screenName);
		t.send(new HitBuilders.AppViewBuilder().build());
	}
	
	public void showNoInternetDialog(Context context) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		alertDialog.setTitle(getString(R.string.my_error_title_no_internet));

		alertDialog.setMessage(getString(R.string.splash_no_internet));

		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
				getString(R.string.my_ok),
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		try {
			alertDialog.show();
		} catch (Exception e) {
			// handle the exception
		}
	}
	
	// =================================================
	// GETTERS AND SETTERS
	// =================================================
	
	// =================================================
	// INNER CLASSES
	// =================================================
	
	public static class ConnectionChangeReceiver extends BroadcastReceiver
	{
	  @Override
	  public void onReceive( Context context, Intent intent )
	  {
	    ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
	    NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
//	    NetworkInfo mobNetInfo = connectivityManager.getNetworkInfo(     ConnectivityManager.TYPE_MOBILE );
	    if ( activeNetInfo != null ) {
	    	isInternetAvailable = true;
	    }
	    else {
	    	isInternetAvailable = false;
	    }
//	    if( mobNetInfo != null )
//	    {
//	      Toast.makeText( context, "Mobile Network Type : " + mobNetInfo.getTypeName(), Toast.LENGTH_SHORT ).show();
//	    }
	  }
	}

	public static boolean pKeyParse(String pKey){
		if(TextUtils.isEmpty(pKey))
			return false;

		try{
			Kxx = pKey.split("xx\\$\\$yy");
		}
		catch (Exception e){
			e.printStackTrace();
		}

		return pKeyIsAvailable();
	}

	public static boolean pKeyIsAvailable(){
		if(Kxx == null || Kxx.length < 2)
			return false;

		return true;
	}
	
}
