package com.learnnet.ashkelon.Adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Objects.TeacherExamData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class TeacherExamsAdapter extends ArrayAdapter<TeacherExamData> {

	public Activity activity;
	public List<TeacherExamData> tweets;

	public TeacherExamsAdapter(Activity a, int textViewResourceId, List<TeacherExamData> tweets) {
		super(a, textViewResourceId, tweets);
		this.tweets = tweets;
		activity = a;
	}

	public static class ViewHolder {
		public TextView courseName;
		public TextView text1;
		public TextView text2;
		public TextView text3;
		public TextView text4;
		public TextView text5;
		public TextView text6;
		public TextView text7;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;
		ViewHolder holder;

		final TeacherExamData tweet = tweets.get(position);

		if (v == null) {

			LayoutInflater vi = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.teacher_exam_list_item, null);
			holder = new ViewHolder();
			holder.courseName = (TextView) v.findViewById(R.id.course_name);
			holder.text1 = (TextView) v.findViewById(R.id.line1);
			holder.text2 = (TextView) v.findViewById(R.id.line2);
			holder.text3 = (TextView) v.findViewById(R.id.line3);
			holder.text4 = (TextView) v.findViewById(R.id.line4);
			holder.text5 = (TextView) v.findViewById(R.id.line5);
			holder.text6 = (TextView) v.findViewById(R.id.line6);
			holder.text7 = (TextView) v.findViewById(R.id.line7);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		if (tweet != null) {
			holder.courseName.setText(tweet.COURSENAME);
			holder.text1.setText(tweet.COURSENUMBER);
			holder.text2.setText(tweet.FIRSTTIME);
			holder.text3.setText(tweet.FIRSTDATE);
			holder.text4.setText(tweet.SECONDTIME);
			holder.text5.setText(tweet.SECONDDATE);
			holder.text6.setText(tweet.THIRDTIME);
			holder.text7.setText(tweet.THIRDDATE);
		}

		return v;
	}

}
