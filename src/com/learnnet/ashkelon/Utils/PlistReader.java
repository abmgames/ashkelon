package com.learnnet.ashkelon.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class PlistReader {

	static int i = 0;
	static String key = "";

	public static ArrayList<Map<String, Object>> Read(InputStream plistStream)
			throws XmlPullParserException, IOException {

		ArrayList<Map<String, Object>> plistParsed = new ArrayList<Map<String, Object>>();
		Map<String, Object> data = new HashMap<String, Object>();

		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser xpp = factory.newPullParser();

		xpp.setInput(plistStream, null);
		xpp.next();
		int eventType = xpp.getEventType();
		int flag1 = 0, flag2 = 0, flag3 = 0;
		while (eventType != XmlPullParser.END_DOCUMENT) {

			if (eventType == XmlPullParser.START_TAG) {

				String mainTag = xpp.getName().toString();
				if (mainTag.equals("dict")) {
					if (flag1 == 0) {
						flag1 = 1;
						i = i + 1;
						data = new HashMap<String, Object>();
					} else {
						flag2 = 1;
					}

				}

				else if (mainTag.equals("key")) {
					flag3 = 1;
				} else if (mainTag.equals("true")) {
					data.put(key, "true");
					flag3 = 0;
				} else if (mainTag.equals("false")) {
					data.put(key, "false");
					flag3 = 0;
				} else {
					flag3 = 2;
				}
			}

			else if (eventType == XmlPullParser.END_TAG) {
				String g = xpp.getName().toString();

				if (g.equals("dict")) {
					if (flag1 == 1 && flag2 != 1) {

						plistParsed.add(data);

						flag1 = 0;
					} else {
						flag2 = 0;
					}

				}
			} else if (eventType == XmlPullParser.TEXT) {
				if (flag3 == 1) {

					key = xpp.getText();

					flag3 = 0;
				} else if (flag3 == 2) {

					data.put(key, xpp.getText());

					flag3 = 0;
				}
			}

			eventType = xpp.next();
		}

		return plistParsed;

	}

	public static List<String> ReadToArrey(InputStream plistStream)
			throws XmlPullParserException, IOException {

		List<String> plistParsed = new ArrayList<String>();
		String data = "";

		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser xpp = factory.newPullParser();

		xpp.setInput(plistStream, null);
		xpp.next();
		int eventType = xpp.getEventType();
		boolean startRead = false;
		while (eventType != XmlPullParser.END_DOCUMENT) {

			if (eventType == XmlPullParser.START_TAG) {

				String mainTag = xpp.getName().toString();
				if (mainTag.equals("string")) {
					startRead = true;
					data = "";
				}
			}else if (eventType == XmlPullParser.END_TAG) {
				String g = xpp.getName().toString();
				if (g.equals("string")) {
					startRead = false;
					plistParsed.add(data);
				}
			}else if (eventType == XmlPullParser.TEXT) {
				if(startRead){
					data = xpp.getText();
				}
			}
			eventType = xpp.next();
		}

		return plistParsed;

	}
	
	public static Map<String, Map<String, Object>> ReadToMap(InputStream plistStream)
			throws XmlPullParserException, IOException {

		Map<String, Map<String, Object>> plistParsed = new HashMap<String, Map<String,Object>>(); 
		Map<String, Object> data = new HashMap<String, Object>();
		String mapName = "";

		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser xpp = factory.newPullParser();

		xpp.setInput(plistStream, null);
		xpp.next();
		int eventType = xpp.getEventType();
		int flag1 = 0, flag2 = 0, flag3 = 0, flag4 = 0;
		while (eventType != XmlPullParser.END_DOCUMENT) {

			if (eventType == XmlPullParser.START_TAG) {

				String mainTag = xpp.getName().toString();
				if (mainTag.equals("dict")) {
					if (flag4 == 0 && key != "") {
						mapName = key;
						flag4 = 1;
					}
					if (flag1 == 0) {
						flag1 = 1;
						i = i + 1;
						data = new HashMap<String, Object>();
					} else {
						flag2 = 1;
					}

				}

				else if (mainTag.equals("key")) {
					flag3 = 1;
				} else if (mainTag.equals("true")) {
					data.put(key, "true");
					flag3 = 0;
				} else if (mainTag.equals("false")) {
					data.put(key, "false");
					flag3 = 0;
				} else {
					flag3 = 2;
				}
			}

			else if (eventType == XmlPullParser.END_TAG) {
				String g = xpp.getName().toString();

				if (g.equals("dict")) {
					if (flag1 == 1 && flag2 != 1) {

						plistParsed.put(mapName, data);
						flag4 = 0;

						flag1 = 0;
					} else {
						flag2 = 0;
					}

				}
			} else if (eventType == XmlPullParser.TEXT) {
				if (flag3 == 1) {

					key = xpp.getText();

					flag3 = 0;
				} else if (flag3 == 2) {

					data.put(key, xpp.getText());

					flag3 = 0;
				}
			}

			eventType = xpp.next();
		}

		return plistParsed;
	}
}
