package com.learnnet.ashkelon.Fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Adapters.MyAcademicMenuListViewAdapter;
import com.learnnet.ashkelon.Communication.MyMichlolRequests;
import com.learnnet.ashkelon.Extensions.MyAcademicMenuManager;
import com.learnnet.ashkelon.Interface.MyInterface;
import com.learnnet.ashkelon.Utils.GlobalDefs;
import com.learnnet.ashkelon.Utils.xmlParser;
import com.learnnet.ashkelon.activity.Exams;
import com.learnnet.ashkelon.activity.Grades;
import com.learnnet.ashkelon.activity.MainMenu;
import com.learnnet.ashkelon.activity.Messages;
import com.learnnet.ashkelon.activity.OnlineEducation;
import com.learnnet.ashkelon.activity.Requests;
import com.learnnet.ashkelon.activity.Schedule;
import com.learnnet.ashkelon.activity.Survey;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class AcademicMenuFragment extends Fragment implements GlobalDefs, OnItemClickListener, MyInterface {

	// =================================================
	// FIELDS
	// =================================================

	private ArrayList<Map<String, String>> studentTableFeaturesArray;
	private String average = "";
	private MyApplication mApp;

	// =================================================
	// CONSTRUCTORS / SINGLETON
	// =================================================

	// =================================================
	// OVERRIDDEN METHODS
	// =================================================

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_academic_menu, container, false);

		mApp = ((MainMenu)getActivity()).getMyApp();

		MyAcademicMenuManager mamm = new MyAcademicMenuManager(getPropertiesInt(PROP_BRANCH_ID), ((MainMenu)getActivity()).getIsTeacher());
		studentTableFeaturesArray = mamm.getMenu();

		ArrayList<Map<String, String>> data = new ArrayList<Map<String,String>>(studentTableFeaturesArray);

		ListView mAcademicList = (ListView) v.findViewById(R.id.lv_academic_menu);
		mAcademicList.setAdapter(new MyAcademicMenuListViewAdapter(((MainMenu)getActivity()), data));
		mAcademicList.setOnItemClickListener(this);

		return v;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Intent i;
		switch (arg2) { // arg2 stands for list position
		case 0:
			// Messages
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), Messages.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 1:
			// Grades
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), Grades.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 2:
			// Exams
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), Exams.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 3:
			// Schedule
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), Schedule.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 4:
			// Average grade
			mApp.sendAnalyticScreen(ANALYTIC_AVERAGE);
			if (MyApplication.isInternetAvailable) {
				showAverageGrade();
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 5:
			// Student requests
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), Requests.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 6:
			// Online education
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), OnlineEducation.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 7:
			// Education survey
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), Survey.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 8:
			// Available Rooms
			if (MyApplication.isInternetAvailable) {
				String roomsLink = "http://www.abmgames.com/__hosted/rooms/index.php?branch=" + getPropertiesInt(PROP_BRANCH_ID);
				Intent intent = new Intent(Intent.ACTION_VIEW);
			    intent.setData(Uri.parse(roomsLink));
			    Intent chooser = Intent.createChooser(intent, getResources().getString(R.string.my_app_chooser));
				startActivity(chooser);	
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		default:
			break;
		}
	}

	@Override
	public boolean isNetworkConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		MyApplication mApp = ((MainMenu)getActivity()).getMyApp();
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			String tempString;
			tempString = (String) mApp.mPropertiesSpecific.get(key);
			tempString = tempString.replaceAll("\\s","");
			if (tempString == "" || tempString == null) {
				return 0;
			}
			else {
				return Integer.valueOf(tempString);
			}
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		// TODO Auto-generated method stub
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			Map<String, Object> map = new HashMap<String, Object>();
			map = (Map<String, Object>) mApp.mPropertiesSpecific.get(key);
			return map;
		}
		else {
			return null;
		}
	}

	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	

	// =================================================
	// CLASS LOGIC
	// =================================================

	private void showAverageGrade() {
		//new GetAverageTask().execute();

		((MainMenu)getActivity()).displaySpinner();

		MyMichlolRequests michlolMessagesRequest = new MyMichlolRequests();

		String username = mApp.mPrefs.getString(PREF_USER_NAME, "");
		String url = mApp.mPrefs.getString(PREF_MICHLOL_URL, "");

		michlolMessagesRequest.call(
			new Response.Listener<String>() {
				@Override
				public void onResponse(String response) {
					xmlParser xp = new xmlParser();
					average = xp.parseAverage(response);

					((MainMenu)getActivity()).hideSpinner();
					((MainMenu)getActivity()).displayAverageDialog(average);
				}
			},
			new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					((MainMenu)getActivity()).hideSpinner();
				}
			},
			REQ_AVERAGE, username, "UnsedValueJustForChecks", null, null, "http://dev.hebrewcalendar.co.il/RashimAPI/", false); // false doesn't matter here.
	}

	/*private String michlolRequestAverage(String username, String url) {
		MyMichlolRequests michlolMessagesRequest = new MyMichlolRequests();
		return michlolMessagesRequest.call(REQ_AVERAGE, username, "UnsedValueJustForChecks",
				null, null, url, false); // false doesn't matter here.
	}*/

}