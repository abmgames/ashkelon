package com.learnnet.ashkelon.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.learnnet.ashkelon.Adapters.StudentsAdapter;
import com.learnnet.ashkelon.Communication.MyMichlolRequests;
import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.Objects.StudentData;
import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Utils.GlobalDefs;
import com.learnnet.ashkelon.Utils.xmlParser;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class ListPresenceActivity extends ActionBarActivity{

    private HashMap<String, Integer> studentsStatus;
    private int meetingId = -1; //1257781

    //Spinner
    //private GetListPresenceTask mission;
    private ProgressDialog spinnerDialog;
    private boolean spinnerShown = false;

    //Finals
    public static final String MEETING_ID_KEY = "meeting_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!MainMenu.getIsTeacher())
            finish();

        setContentView(R.layout.activity_list_presence);

        //Get extras
        try{
            Bundle extras = getIntent().getExtras();
            meetingId = Integer.valueOf(extras.getString(MEETING_ID_KEY));
        }
        catch (Exception e){
            e.printStackTrace();
        }

        if(meetingId < 0)
        {
            Toast.makeText(getBaseContext(), getString(R.string.missing_details), Toast.LENGTH_SHORT).show();
            finish();

            return;
        }

        this.studentsStatus = new HashMap<String, Integer>();

        // Inflate your custom action bar layout
        final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
                R.layout.action_bar_centered_text, null);

        // Disable drawer swipe gesture
        DrawerLayout mDrawerLayout;
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        // Set up your ActionBar
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setCustomView(actionBarLayout);

        // Enable ActionBar app icon to behave as action to toggle nav drawer
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayUseLogoEnabled(false);
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);

        // Set ActionBar's text
        TextView actionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
        actionBarTitle.setText(getResources().getString(R.string.list_presence));

        /*mission = new GetListPresenceTask();
        mission.execute();*/

        MyMichlolRequests michlolRequests = new MyMichlolRequests();

        String username = MyApplication.getInstance().mPrefs.getString(GlobalDefs.PREF_USER_NAME, "");
        String url = MyApplication.getInstance().mPrefs.getString(GlobalDefs.PREF_MICHLOL_URL, "");

        michlolRequests.call(
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        xmlParser xp = new xmlParser();

                        final LinkedList<StudentData> result = xp.parseStudentsList(response);
                        hideSpinner();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                buildList(result);
                            }
                        });
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideSpinner();
                    }
                },
                GlobalDefs.REQ_LIST_PRESENCE, username, "UnsedValueJustForChecks", michlolRequests.getHebYear(), null, url, false, meetingId, null
        );
    }

    private void buildList(LinkedList<StudentData> studentsList){
        final StudentsAdapter presenceAdapter = new StudentsAdapter(this, studentsList);
        ListView studentsListView = (ListView) findViewById(R.id.students_list);

        studentsListView.setAdapter(presenceAdapter);

        studentsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final StudentData studentDetails = presenceAdapter.getItem(i);
                final StudentsAdapter.ViewHolder viewHolder = (StudentsAdapter.ViewHolder) view.getTag();

                int studentStatus = 0;

                try{
                    studentStatus = studentsStatus.get(studentDetails.STUDENTID);
                }
                catch (Exception e){}

                if(studentStatus < 1){
                    viewHolder.vIcon.setVisibility(View.GONE);
                    viewHolder.buttonsContainer.setVisibility(View.VISIBLE);

                    setLate(viewHolder.absenceButton, viewHolder.lateButton, studentDetails);
                }
                else{
                    viewHolder.vIcon.setVisibility(View.VISIBLE);
                    viewHolder.buttonsContainer.setVisibility(View.GONE);

                    studentsStatus.put(studentDetails.STUDENTID, 0);
                }

                viewHolder.absenceButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setMinus(viewHolder.absenceButton, viewHolder.lateButton, studentDetails);
                    }
                });

                //Late button onClick
                viewHolder.lateButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setLate(viewHolder.absenceButton, viewHolder.lateButton, studentDetails);
                    }
                });
            }
        });
    }

    private void setLate(TextView absenceButton, TextView lateButton, StudentData studentData){
        setButtonStyle(absenceButton, false);
        setButtonStyle(lateButton, true);

        studentsStatus.put(studentData.STUDENTID, 1);
    }

    private void setMinus(TextView absenceButton, TextView lateButton, StudentData studentData){
        setButtonStyle(absenceButton, true);
        setButtonStyle(lateButton, false);

        studentsStatus.put(studentData.STUDENTID, 2);
    }

    private void setButtonStyle(TextView button, boolean selected){

        GradientDrawable shapeDrawable = (GradientDrawable) button.getBackground();

        if(selected){
            shapeDrawable.setColor(getResources().getColor(R.color.select_menu_border));
            button.setTextColor(Color.WHITE);
        }
        else
        {
            shapeDrawable.setColor(Color.WHITE);
            button.setTextColor(getResources().getColor(R.color.select_menu_border));
        }

    }

    /*private String michlolRequestListPresence(String username, String url, int meetingId)
    {
        MyMichlolRequests michlolRequests = new MyMichlolRequests();

        return michlolRequests.call(
                GlobalDefs.REQ_LIST_PRESENCE,
                username,
                "UnsedValueJustForChecks",
                michlolRequests.getHebYear(),
                null,
                url,
                false,
                meetingId,
                null
        );
    }*/



    private String buildPresenceList(){
        StringBuilder presenceList = new StringBuilder();

        int totalStudents  = studentsStatus.size() - 1;
        int currentStudent = 0;

        for(Map.Entry<String, Integer> entry : studentsStatus.entrySet())
        {
            if(entry.getValue() > 0)
            {
                presenceList.append(entry.getKey() + "#");

                if(entry.getValue() == 1)
                    presenceList.append("L");
                else
                    presenceList.append("M");

                if(currentStudent < totalStudents)
                    presenceList.append("*");
            }

            currentStudent++;
        }

        return presenceList.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.presence_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.send:

                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();

                        displaySpinner();
                    }

                    @Override
                    protected Void doInBackground(Void... voids) {
                        //Build presence list
                        String presenceList = buildPresenceList();


                        //Send presence list
                        MyMichlolRequests michlolRequests = new MyMichlolRequests();

                        String username = MyApplication.getInstance().mPrefs.getString(GlobalDefs.PREF_USER_NAME, "");
                        String url = MyApplication.getInstance().mPrefs.getString(GlobalDefs.PREF_MICHLOL_URL, "");

                        michlolRequests.call(
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                    }
                                },
                                GlobalDefs.REQ_ADMISSIONS,
                                username,
                                "UnsedValueJustForChecks",
                                michlolRequests.getHebYear(),
                                null,
                                url,
                                false,
                                meetingId,
                                presenceList
                        );
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);

                        hideSpinner();

                        Toast.makeText(ListPresenceActivity.this, getString(R.string.successfully_sent), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }.execute();


                break;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Log.i("YONI", buildPresenceList());
    }

    public void displaySpinner() {

        if (!spinnerShown) {

            try {
                spinnerDialog = ProgressDialog.show(
                        this,
                        "",
                        getString(R.string.my_wait),
                        true,
                        true,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                try {
                                    //mission.cancel(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                finish();
                                spinnerShown = false;
                            }
                        }
                );
            } catch (Exception e) {
                e.printStackTrace();
            }

            spinnerShown = true;
        }
    }

    public void hideSpinner() {
        try {
            spinnerDialog.hide();
            spinnerDialog = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        spinnerShown = false;
    }

}
