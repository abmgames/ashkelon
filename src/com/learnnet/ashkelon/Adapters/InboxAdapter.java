package com.learnnet.ashkelon.Adapters;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Objects.InboxData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class InboxAdapter extends ArrayAdapter<InboxData> {

	private final List<InboxData> list;
	private final Activity context;

	public InboxAdapter(Activity context, List<InboxData> list) {
		super(context, R.layout.inbox_list_item, list);
		this.context = context;
		this.list = list;
	}

	private class ViewHolder {
		protected TextView title;
		protected TextView date;
		protected CheckBox checkbox;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;
		if (convertView == null) {
			LayoutInflater inflator = context.getLayoutInflater();
			view = inflator.inflate(R.layout.inbox_list_item, null);
			final ViewHolder viewHolder = new ViewHolder();
			viewHolder.title = (TextView) view.findViewById(R.id.line1);
			viewHolder.date = (TextView) view.findViewById(R.id.line2);
			viewHolder.checkbox = (CheckBox) view.findViewById(R.id.message_checkbox);
			viewHolder.checkbox
			.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					InboxData element = (InboxData) viewHolder.checkbox
							.getTag();
					element.setSelected(buttonView.isChecked());

				}
			});
			view.setTag(viewHolder);
			viewHolder.checkbox.setTag(list.get(position));
		} else {
			view = convertView;
			((ViewHolder) view.getTag()).checkbox.setTag(list.get(position));
		}
		ViewHolder holder = (ViewHolder) view.getTag();
		holder.title.setText(list.get(position).title);
		holder.date.setText(list.get(position).date);
		holder.checkbox.setChecked(list.get(position).isSelected);
		return view;
	}
}
