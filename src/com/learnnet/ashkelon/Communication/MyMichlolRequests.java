package com.learnnet.ashkelon.Communication;

import android.text.TextUtils;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.learnnet.ashkelon.HebrewCalendar.JewishCalendar;
import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.Utils.xmlParser;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class MyMichlolRequests {

	private static final String RASHIM_DEMO_API_URL = "http://rashim.co.il/wsm3api/michlolapi.asmx";
	private static final List<Integer> ATTACH_DATA_REQUESTS = new ArrayList<Integer>(Arrays.asList(new Integer[]{
			4,  //Grades
			19, //Messages
			27, //SCHEDULE
			28, //Exams
			32, //Student requests
	}));

	public void call(Response.Listener<String> respListener, Response.ErrorListener errorListener, String reqID, String user, String password,
					   String option1, String option2, String kMichlolApiURL, boolean isUsingIdm){

		call(respListener, errorListener, reqID, user, password, option1, option2, kMichlolApiURL, isUsingIdm, 0, "");
	}

	public void call(final Response.Listener<String> respListener, final Response.ErrorListener errorListener, final String reqID, final String user, final String password,
					   final String option1, final String option2, String kMichlolApiURL, final boolean isUsingIdm, final int meetingId, final String admissions) {

		if(!MyApplication.pKeyIsAvailable())
			respListener.onResponse("");


		if (!checkValidation(reqID) || !checkValidation(user)
				|| !checkValidation(password)) {
			respListener.onResponse("");
		}

		StringRequest req = new StringRequest(Request.Method.POST,
				MyApplication.DEBUG_MODE ? RASHIM_DEMO_API_URL : kMichlolApiURL,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						try{
							xmlParser xp = new xmlParser();

							InputStream stream = new ByteArrayInputStream(response.getBytes());
							respListener.onResponse(xp.parseOutputData(new DataInputStream(stream)));
						}
						catch (Exception e){
							errorListener.onErrorResponse(new VolleyError(e));
						}
					}
				},
				errorListener) {

			@Override
			public byte[] getBody() throws com.android.volley.AuthFailureError {

				String body = soapRequest(body(
						Integer.valueOf(reqID), user, password, option1, option2, isUsingIdm, meetingId, admissions),Integer.valueOf(reqID));

				/*try{
					Log.i("Volley", body);
					return new String(body.getBytes(), "UTF-8").getBytes();
				}
				catch (Exception e){
					return "".getBytes();
				}*/

				return body.getBytes(Charset.forName("UTF-8"));
			}

			@Override
			public String getBodyContentType()
			{
				return "text/xml; charset=utf-8";
			}

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				//Map<String, String> headers = super.getHeaders();

				Map<String, String> params = new HashMap<>();

				params.put("SOAPAction", "http://RashimApi.co.il/ProcessRequest");
				params.put("Content-Type", "text/xml; charset=utf-8");

				return params;
			}

		};

		MyApplication.getInstance().addToRequestQueue(req);


		/*HttpClient httpclient = getNewHttpClient();

		HttpPost httppost = new HttpPost(MyApplication.DEBUG_MODE ? RASHIM_DEMO_API_URL : kMichlolApiURL);

		httppost.setHeader("SOAPAction", "http://RashimApi.co.il/ProcessRequest");
		httppost.setHeader("Content-Type", "text/xml; charset=utf-8");

		try {

			// String temp =
			// soapRequest(body(Integer.valueOf(reqID),user,password,option1,option2));

			HttpEntity entity = new StringEntity(soapRequest(body(
					Integer.valueOf(reqID), user, password, option1, option2, isUsingIdm, meetingId, admissions),Integer.valueOf(reqID)),
					HTTP.UTF_8);
			httppost.setEntity(entity);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity r_entity = response.getEntity();

			if (r_entity.isStreaming() && r_entity != null) {

				// soapXMLParser soapParser = new soapXMLParser();
				// result = soapParser.parse(new
				// DataInputStream(r_entity.getContent()));
				xmlParser xp = new xmlParser();
				result.append(xp.parseOutputData(new DataInputStream(r_entity
						.getContent())));

//				Log.e("result:", result.toString());
			}
			httpclient.getConnectionManager().shutdown();

		} catch (Exception E) {
			return null;
		}

		return result.toString();*/
	}

	private String soapRequest(String body , int reqID) {
		final StringBuffer soap = new StringBuffer(); // Comment: 4
		soap.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
		soap.append("<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n");
		soap.append(	"<SOAP:Body>\n");
		soap.append(		"<ProcessRequest xmlns=\"http://RashimApi.co.il\">\n");
		soap.append(			"<P_RequestParams>");
		soap.append(				body);
		soap.append(			"</P_RequestParams>");
		soap.append(			"<Authenticator>");
		soap.append(				"<UserName>" + MyApplication.Kxx[0] + "</UserName>");
		soap.append(				"<Password>" + MyApplication.Kxx[1] + "</Password>");
		soap.append(			"</Authenticator>");
		soap.append(		"</ProcessRequest>\n");
		soap.append(	"</SOAP:Body>\n");
		soap.append("</SOAP:Envelope>");

		String soapRequest = soap.toString();

		if(MyApplication.DEBUG_MODE)
			Log.i("DEBUG", soapRequest);

		return soapRequest;

	}

	private String body(int reqID, String user, String password,
						String option1, String option2, boolean isUsingIdm, int meetingId, String admissions) {

		// user = String.valueOf(Integer.valueOf(user));

		StringBuffer body = new StringBuffer();

		body.append("<RequestID>" + String.valueOf(reqID) + "</RequestID>");
		body.append("<InputData>");

		body.append("&lt;?xml version=\"1.0\" encoding=\"utf-8\" ?&gt;\n");
		body.append("&lt;PARAMS&gt;\n");

		if(reqID != 48 && !TextUtils.isEmpty(MyApplication.TOKEN))
		{
			body.append("&lt;TOKEN&gt;" + MyApplication.TOKEN + "&lt;/TOKEN&gt;\n");

			if(ATTACH_DATA_REQUESTS.contains(reqID)){
				body.append("&lt;USERNAME&gt;" + MyApplication.pU + "&lt;/USERNAME&gt;\n");
				body.append("&lt;PASSWORD&gt;" + MyApplication.pP + "&lt;/PASSWORD&gt;\n");
			}
		}

		switch (reqID) {
			case 48: //Login
				if (isUsingIdm) {
					//Only for achva
					body.append("&lt;ZHT&gt;" + user + "&lt;/ZHT&gt;\n");
					body.append("&lt;PASSWORD&gt;&lt;/PASSWORD&gt;\n");
					body.append("&lt;SNL&gt;" + getHebYear() + "&lt;/SNL&gt;\n");
					body.append("&lt;USERNAME&gt;" + password + "&lt;/USERNAME&gt;\n");
					body.append("&lt;RETHASIMA&gt;1&lt;/RETHASIMA&gt;\n");
				}
				else {
					body.append("&lt;ZHT&gt;" + user + "&lt;/ZHT&gt;\n");
					body.append("&lt;USERNAME&gt;&lt;/USERNAME&gt;\n");

					//Only for achva!
					//body.append("&lt;USERNAME&gt;" + user + "&lt;/USERNAME&gt;\n");
					//body.append("&lt;ZHT&gt;&lt;/ZHT&gt;\n");

					body.append("&lt;PASSWORD&gt;" + password + "&lt;/PASSWORD&gt;\n");
					body.append("&lt;SNL&gt;" + getHebYear() + "&lt;/SNL&gt;\n");

					body.append("&lt;RETHASIMA&gt;1&lt;/RETHASIMA&gt;\n");
				}
				break;
			case 4: //Grades
				body.append("&lt;STUDENT_ID&gt;" + user + "&lt;/STUDENT_ID&gt;\n");
				body.append("&lt;YEAR&gt;" + option1 + "&lt;/YEAR&gt;\n");
				break;
			case 19: //Messages
				body.append("&lt;STUDENT_ID&gt;" + user + "&lt;/STUDENT_ID&gt;\n");
				body.append("&lt;MSG_TYPE&gt;0&lt;/MSG_TYPE&gt;\n");
				break;
			case 20:
				body.append("&lt;STUDENT_ID&gt;" + user + "&lt;/STUDENT_ID&gt;\n");
				body.append("&lt;MARKS_COUNT&gt;" + 50 + "&lt;/MARKS_COUNT&gt;\n");
				break;
			case 23: //TEACHER SCHEDULE
				body.append("&lt;ZHT&gt;" + user + "&lt;/ZHT&gt;\n");
				body.append("&lt;SNL&gt;" + getHebYear() + "&lt;/SNL&gt;\n");
				break;
			case 27: //SCHEDULE
				body.append("&lt;STUDENTID&gt;" + user + "&lt;/STUDENTID&gt;\n");
				body.append("&lt;BEGINDATE&gt;" + option1 + "&lt;/BEGINDATE&gt;\n");
				body.append("&lt;ENDDATE&gt;" + option2 + "&lt;/ENDDATE&gt;\n");
				break;
			case 28: //Exams
				body.append("&lt;STUDENTID&gt;" + user + "&lt;/STUDENTID&gt;\n");
				body.append("&lt;YEAR&gt;" + getHebYear() + "&lt;/YEAR&gt;\n");
				body.append("&lt;CATEGORY&gt;" + option1 + "&lt;/CATEGORY&gt;\n");
				break;
			case 32: //Student requests
				body.append("&lt;STUDENTID&gt;" + user + "&lt;/STUDENTID&gt;\n");
				break;
			case 36:
				body.append("&lt;STUDENT_ID&gt;" + user + "&lt;/STUDENT_ID&gt;\n");
				body.append("&lt;COURSE_ID&gt;" + option1 + "&lt;/COURSE_ID&gt;\n");
				break;
			case 37:
				body.append("&lt;STUDENT_ID&gt;" + user + "&lt;/STUDENT_ID&gt;\n");
				break;
			case 39:
				body.append("&lt;STUDENT_ID&gt;" + user + "&lt;/STUDENT_ID&gt;\n");
				body.append("&lt;Year&gt;" + getHebYear() + "&lt;/Year&gt;\n");
				break;
			case 49:
				body.append("&lt;TEACHER_ID&gt;" + user + "&lt;/TEACHER_ID&gt;\n");
				body.append("&lt;YEAR&gt;" + getHebYear() + "&lt;/YEAR&gt;\n");
				break;
			case 52:
				body.append("&lt;TEACHERID&gt;" + user + "&lt;/TEACHERID&gt;\n");
				body.append("&lt;DATE&gt;" + option1 + "&lt;/DATE&gt;\n");
				body.append("&lt;TODATE&gt;" + option2 + "&lt;/TODATE&gt;\n");

				break;
			case 63:
				body.append("&lt;MEETINGID&gt;" + meetingId + "&lt;/MEETINGID&gt;\n");
				body.append("&lt;INCLUDEIMAGE&gt;1&lt;/INCLUDEIMAGE&gt;\n");
				body.append("&lt;ZHT&gt;" + user + "&lt;/ZHT&gt;\n");

				break;

			case 64:
				body.append("&lt;MEETING_ID&gt;" + meetingId + "&lt;/MEETING_ID&gt;\n");
				body.append("&lt;ADMISIONS&gt;" + admissions + "&lt;/ADMISIONS&gt;\n");
				body.append("&lt;ZHT&gt;" + user + "&lt;/ZHT&gt;\n");

				break;
			default:
				break;
		}

		body.append("&lt;/PARAMS&gt;\n");
		body.append("</InputData>");

		return body.toString();
	}

	private Boolean checkValidation(String testString) {
		if (testString.contains("<") || testString.contains(">")
				|| testString.contains("\"") || testString.contains("\'")
				|| (testString == "")) {
			return false;
		} else {
			return true;
		}

	}

	public String getHebYear() {

		JewishCalendar jc = new JewishCalendar(new Date());

		int year = jc.getJewishYear();

		Map<String, String> map = new HashMap<String, String>();

		map.put("1", "א");
		map.put("2", "ב");
		map.put("3", "ג");
		map.put("4", "ד");
		map.put("5", "ה");
		map.put("6", "ו");
		map.put("7", "ז");
		map.put("8", "ח");
		map.put("9", "ט");
		map.put("10", "י");
		map.put("20", "כ");
		map.put("30", "ל");
		map.put("40", "מ");
		map.put("50", "נ");
		map.put("60", "ס");
		map.put("70", "ע");
		map.put("80", "פ");
		map.put("90", "צ");
		map.put("100", "ק");
		map.put("200", "ר");
		map.put("300", "ש");
		map.put("400", "ת");

		year = year % 1000;

		StringBuilder yearString = new StringBuilder();

		while (year > 0) {
			// Get Max Value
			int currentValue = 400;
			while ((currentValue > year) && (currentValue > 100)) {
				currentValue -= 100;
			}
			while ((currentValue > year) && (currentValue > 10)) {
				currentValue -= 10;
			}
			while ((currentValue > year) && (currentValue > 0)) {
				currentValue -= 1;
			}
			yearString.append(map.get(String.valueOf(currentValue)));
			year -= currentValue;
		}

		return yearString.toString();

	}

}
