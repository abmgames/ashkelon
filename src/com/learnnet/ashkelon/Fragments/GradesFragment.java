package com.learnnet.ashkelon.Fragments;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Adapters.GradesAdapter;
import com.learnnet.ashkelon.Communication.MyMichlolRequests;
import com.learnnet.ashkelon.Interface.MyInterface;
import com.learnnet.ashkelon.Objects.GradeData;
import com.learnnet.ashkelon.Utils.GlobalDefs;
import com.learnnet.ashkelon.Utils.xmlParser;
import com.learnnet.ashkelon.activity.Grades;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class GradesFragment extends Fragment implements GlobalDefs, MyInterface, OnItemSelectedListener, OnItemClickListener {
	
	// =================================================
	// FIELDS
	// =================================================
	
	private ListView mGradesListView;
	private GradesAdapter mGradesAdapter;
	private List<GradeData> mData;
	private MyApplication mApp;
	private SpinnerAdapter mSpinnerAdapter;
	private Spinner spinner;
	private ArrayList<String> studentYears;
	
	// =================================================
	// CONSTRUCTORS / SINGLETON
	// =================================================
	
	// =================================================
	// OVERRIDDEN METHODS
	// =================================================
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_grades, container, false);
		
		mGradesListView = (ListView) v.findViewById(R.id.lv_grades);
		
		mApp = ((Grades)getActivity()).getMyApp();
		
		mData = ((Grades)getActivity()).getGradesData();
		
		mGradesListView.setOnItemClickListener(this);
		
		studentYears = new ArrayList<String>();
		for (int i=0 ; i<mApp.mCurrentUserYears.size() ; i++) {
			studentYears.add(mApp.mCurrentUserYears.get(i).getYear());
		}

		// Set up spinner
		spinner = (Spinner) v.findViewById(R.id.spinner1);
		mSpinnerAdapter = ArrayAdapter.createFromResource((Grades)getActivity(), R.array.my_array_exams_menu,
				android.R.layout.simple_spinner_dropdown_item);
		mSpinnerAdapter = new ArrayAdapter<String>((Grades)getActivity(), android.R.layout.simple_spinner_dropdown_item, studentYears);

		spinner.setAdapter(mSpinnerAdapter);
		spinner.setOnItemSelectedListener(this);

		return v;
	}

	@Override
	public boolean isNetworkConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			String tempString;
			tempString = (String) mApp.mPropertiesSpecific.get(key);
			tempString = tempString.replaceAll("\\s","");
			if (tempString == "" || tempString == null) {
				return 0;
			}
			else {
				return Integer.valueOf(tempString);
			}
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		String year = mApp.mCurrentUserYears.get(arg2).getYear();

		try{
			//year = URLEncoder.encode(year, "UTF-8");
			//year = URLDecoder.decode(URLEncoder.encode(year, "Windows-1255"), "UTF-8");

		}
		catch (Exception e){}

		if (MyApplication.isInternetAvailable)
		{
			((Grades)getActivity()).displaySpinner();

			//new GetGradesTask().execute(year);
			String username = mApp.mPrefs.getString(PREF_USER_NAME, "");
			String url = mApp.mPrefs.getString(PREF_MICHLOL_URL, "");

			MyMichlolRequests michlolMessagesRequest = new MyMichlolRequests();

			/*michlolMessagesRequest.call(
					new Response.Listener<String>() {
						@Override
						public void onResponse(String response) {
							Log.i("df", response);
						}
					},
					new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							((Grades)getActivity()).hideSpinner();
						}
					},
					REQ_GRADES, username, "UnsedValueJustForChecks", year, null, "http://dev.hebrewcalendar.co.il/RashimAPI/", false); // false doesn't matter here.*/

			michlolMessagesRequest.call(
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						xmlParser xp = new xmlParser();
						mData = xp.parseGradesData(response);

						Collections.sort(mData);

						((Grades)getActivity()).setGradesData(mData);
						showList();
						((Grades)getActivity()).hideSpinner();
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						((Grades)getActivity()).hideSpinner();
					}
				},
			REQ_GRADES, username, "UnsedValueJustForChecks", year, null, "http://dev.hebrewcalendar.co.il/RashimAPI/", false); // false doesn't matter here.
		}
		else {
			mApp.showNoInternetDialog((Grades)getActivity());
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		((Grades)getActivity()).getGradeDetails(mData.get(arg2));
	}
	
	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	
	
	// =================================================
	// CLASS LOGIC
	// =================================================
	
	/*private String michlolRequestGrades(String username, String url, String year) {
		MyMichlolRequests michlolMessagesRequest = new MyMichlolRequests();
		return michlolMessagesRequest.call(REQ_GRADES, username, "UnsedValueJustForChecks",
				year, null, url, false); // false doesn't matter here.
	}*/
	
	private void showList() {
		mGradesAdapter = new GradesAdapter((Grades)getActivity(), R.layout.grade_list_item, mData);
		mGradesListView.setAdapter(mGradesAdapter);
		mGradesAdapter.notifyDataSetChanged();
	}

}
