package com.learnnet.ashkelon.Extensions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.learnnet.ashkelon.Utils.GlobalDefs;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class MyAcademicMenuManager implements GlobalDefs {

	private ArrayList<Map<String, String>> menu;

	public MyAcademicMenuManager(int instituteBranchID, boolean isTeacher) {

		menu = new ArrayList<Map<String,String>>();

		switch (instituteBranchID) {

		case 1:
			initTelHaiMenu(isTeacher);
			break;
		case 2:
			initAshkelonMenu(isTeacher);
			break;
		case 3:
			initRuppinMenu(isTeacher);
			break;
		case 4:
			initHitMenu(isTeacher);
			break;
		case 5:
			initRuppinMenu(isTeacher);
			break;
		case 6:
			initRuppinMenu(isTeacher);
			break;
		case 7:
			initHemdatMenu(isTeacher);
			break;
		case 8:
			initShankarMenu(isTeacher);
			break;
		case 9:
			initRuppinMenu(isTeacher);
			break;
		case 10:
			initRuppinMenu(isTeacher);
			break;
		default:
			initRuppinMenu(isTeacher);
			break;
		}
	}

	private void initRuppinMenu(boolean isTeacher) {

		if (isTeacher) {
			final String[] titles = {"שיעורים", "מבחנים", "הוראה מקוונת"};
			final String[] descs = {"רשימת שיעורים למרצה", "מבחנים לאיש סגל", ""};
			final String[] imgs = {"academic_ic_schedule", "academic_ic_exams", "academic_ic_portal"};
			for (int i=0 ; i<titles.length ; i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(MENU_TITLE, titles[i]);
				map.put(MENU_DESC, descs[i]);
				map.put(MENU_IMG, imgs[i]);
				menu.add(map);
			}
		}
		else {
			final String[] titles = {"הודעות", "ציונים", "בחינות", "מערכת שעות", "ממוצע", "בקשות", "הוראה מקוונת", "סקר הוראה"};
			final String[] descs = {"הודעות מחלקה אקדמית", "רשימת ציונים אחרונים", "רשימת בחינות לפי חלוקה", "מערכת שעות שבועית",
					"ממוצע משוקלל מצטבר", "בקשות סטודנט קיימות במידה וקיימות במערכת", "", ""};
			final String[] imgs = {"academic_ic_messages", "academic_ic_grades", "academic_ic_exams", "academic_ic_schedule",
					"academic_ic_average", "academic_ic_requests", "academic_ic_portal", "academic_ic_surveys"};
			for (int i=0 ; i<titles.length ; i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(MENU_TITLE, titles[i]);
				map.put(MENU_DESC, descs[i]);
				map.put(MENU_IMG, imgs[i]);
				menu.add(map);
			}
		}
	}

	private void initAshkelonMenu(boolean isTeacher) {

		if (isTeacher) {
			final String[] titles = {"הודעות", "שיעורים", "מבחנים", "הוראה מקוונת", "מערכת שעות"};
			final String[] descs = {"הודעות מחלקה אקדמית", "רשימת שיעורים למרצה", "מבחנים לאיש סגל", "", "מערכת שעות שבועית"};
			final String[] imgs = {"academic_ic_messages", "academic_ic_schedule", "academic_ic_exams", "academic_ic_portal", "academic_ic_schedule"};
			for (int i=0 ; i<titles.length ; i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(MENU_TITLE, titles[i]);
				map.put(MENU_DESC, descs[i]);
				map.put(MENU_IMG, imgs[i]);
				menu.add(map);
			}
		}
		else {
			final String[] titles = {"הודעות", "ציונים", "בחינות", "מערכת שעות", "ממוצע", "בקשות", "הוראה מקוונת", "סקר הוראה", "כיתות מחשב פנויות"};
			final String[] descs = {"הודעות מחלקה אקדמית", "רשימת ציונים אחרונים", "רשימת בחינות לפי חלוקה", "מערכת שעות שבועית",
					"ממוצע משוקלל מצטבר", "בקשות סטודנט קיימות במידה וקיימות במערכת", "", "", ""};
			final String[] imgs = {"academic_ic_messages", "academic_ic_grades", "academic_ic_exams", "academic_ic_schedule",
					"academic_ic_average", "academic_ic_requests", "academic_ic_portal", "academic_ic_surveys", "academic_ic_rooms"};
			for (int i=0 ; i<titles.length ; i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(MENU_TITLE, titles[i]);
				map.put(MENU_DESC, descs[i]);
				map.put(MENU_IMG, imgs[i]);
				menu.add(map);
			}
		}
	}
	
	private void initTelHaiMenu(boolean isTeacher) {

		if (isTeacher) {
			final String[] titles = {"שיעורים", "מבחנים", "הוראה מקוונת"};
			final String[] descs = {"רשימת שיעורים למרצה", "מבחנים לאיש סגל", ""};
			final String[] imgs = {"academic_ic_schedule", "academic_ic_exams", "academic_ic_portal"};
			for (int i=0 ; i<titles.length ; i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(MENU_TITLE, titles[i]);
				map.put(MENU_DESC, descs[i]);
				map.put(MENU_IMG, imgs[i]);
				menu.add(map);
			}
		}
		else {
			final String[] titles = {"הודעות", "ציונים", "בחינות", "מערכת שעות", "ממוצע", "בקשות", "הוראה מקוונת", "סקר הוראה"};
			final String[] descs = {"הודעות מחלקה אקדמית", "רשימת ציונים אחרונים", "רשימת בחינות לפי חלוקה", "מערכת שעות שבועית",
					"ממוצע משוקלל מצטבר", "בקשות סטודנט קיימות במידה וקיימות במערכת", "", ""};
			final String[] imgs = {"academic_ic_messages", "academic_ic_grades", "academic_ic_exams", "academic_ic_schedule",
					"academic_ic_average", "academic_ic_requests", "academic_ic_portal", "academic_ic_surveys"};
			for (int i=0 ; i<titles.length ; i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(MENU_TITLE, titles[i]);
				map.put(MENU_DESC, descs[i]);
				map.put(MENU_IMG, imgs[i]);
				menu.add(map);
			}
		}
	}
	
	private void initHitMenu(boolean isTeacher) {

		if (isTeacher) {
			final String[] titles = {"שיעורים", "מבחנים", "הוראה מקוונת"};
			final String[] descs = {"רשימת שיעורים למרצה", "מבחנים לאיש סגל", ""};
			final String[] imgs = {"academic_ic_schedule", "academic_ic_exams", "academic_ic_portal"};
			for (int i=0 ; i<titles.length ; i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(MENU_TITLE, titles[i]);
				map.put(MENU_DESC, descs[i]);
				map.put(MENU_IMG, imgs[i]);
				menu.add(map);
			}
		}
		else {
			final String[] titles = {"הודעות", "ציונים", "בחינות", "מערכת שעות", "ממוצע", "בקשות", "הוראה מקוונת", "סקר הוראה"};
			final String[] descs = {"הודעות מחלקה אקדמית", "רשימת ציונים אחרונים", "רשימת בחינות לפי חלוקה", "מערכת שעות שבועית",
					"ממוצע משוקלל מצטבר", "בקשות סטודנט קיימות במידה וקיימות במערכת", "", ""};
			final String[] imgs = {"academic_ic_messages", "academic_ic_grades", "academic_ic_exams", "academic_ic_schedule",
					"academic_ic_average", "academic_ic_requests", "academic_ic_portal", "academic_ic_surveys"};
			for (int i=0 ; i<titles.length ; i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(MENU_TITLE, titles[i]);
				map.put(MENU_DESC, descs[i]);
				map.put(MENU_IMG, imgs[i]);
				menu.add(map);
			}
		}
	}
	
	private void initShankarMenu(boolean isTeacher) {

		if (isTeacher) {
			final String[] titles = {"שיעורים", "מבחנים", "הוראה מקוונת"};
			final String[] descs = {"רשימת שיעורים למרצה", "מבחנים לאיש סגל", ""};
			final String[] imgs = {"academic_ic_schedule", "academic_ic_exams", "academic_ic_portal"};
			for (int i=0 ; i<titles.length ; i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(MENU_TITLE, titles[i]);
				map.put(MENU_DESC, descs[i]);
				map.put(MENU_IMG, imgs[i]);
				menu.add(map);
			}
		}
		else {
			final String[] titles = {"הודעות", "ציונים", "בחינות", "מערכת שעות", "ממוצע", "בקשות", "הוראה מקוונת", "סקר הוראה"};
			final String[] descs = {"הודעות מחלקה אקדמית", "רשימת ציונים אחרונים", "רשימת בחינות לפי חלוקה", "מערכת שעות שבועית",
					"ממוצע משוקלל מצטבר", "בקשות סטודנט קיימות במידה וקיימות במערכת", "", ""};
			final String[] imgs = {"academic_ic_messages", "academic_ic_grades", "academic_ic_exams", "academic_ic_schedule",
					"academic_ic_average", "academic_ic_requests", "academic_ic_portal", "academic_ic_surveys"};
			for (int i=0 ; i<titles.length ; i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(MENU_TITLE, titles[i]);
				map.put(MENU_DESC, descs[i]);
				map.put(MENU_IMG, imgs[i]);
				menu.add(map);
			}
		}
	}
	
	private void initHemdatMenu(boolean isTeacher) {

		if (isTeacher) {
			final String[] titles = {"שיעורים", "מבחנים", "הוראה מקוונת"};
			final String[] descs = {"רשימת שיעורים למרצה", "מבחנים לאיש סגל", ""};
			final String[] imgs = {"academic_ic_schedule", "academic_ic_exams", "academic_ic_portal"};
			for (int i=0 ; i<titles.length ; i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(MENU_TITLE, titles[i]);
				map.put(MENU_DESC, descs[i]);
				map.put(MENU_IMG, imgs[i]);
				menu.add(map);
			}
		}
		else {
			final String[] titles = {"הודעות", "ציונים", "בחינות", "מערכת שעות", "ממוצע", "בקשות", "הוראה מקוונת", "סקר הוראה"};
			final String[] descs = {"הודעות מחלקה אקדמית", "רשימת ציונים אחרונים", "רשימת בחינות לפי חלוקה", "מערכת שעות שבועית",
					"ממוצע משוקלל מצטבר", "בקשות סטודנט קיימות במידה וקיימות במערכת", "", ""};
			final String[] imgs = {"academic_ic_messages", "academic_ic_grades", "academic_ic_exams", "academic_ic_schedule",
					"academic_ic_average", "academic_ic_requests", "academic_ic_portal", "academic_ic_surveys"};
			for (int i=0 ; i<titles.length ; i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(MENU_TITLE, titles[i]);
				map.put(MENU_DESC, descs[i]);
				map.put(MENU_IMG, imgs[i]);
				menu.add(map);
			}
		}
	}

	public ArrayList<Map<String, String>> getMenu() {
		return menu;
	}
}
