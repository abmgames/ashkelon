package com.learnnet.ashkelon.activity;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Communication.MyIdmLoginRequest;
import com.learnnet.ashkelon.Communication.MyMichlolRequests;
import com.learnnet.ashkelon.Interface.MyInterface;
import com.learnnet.ashkelon.Objects.IdmLoginData;
import com.learnnet.ashkelon.Objects.UserDetailsData;
import com.learnnet.ashkelon.Utils.GlobalDefs;
import com.learnnet.ashkelon.Utils.xmlParser;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class Login extends Activity implements GlobalDefs, MyInterface {
	
	// =================================================
	// FIELDS
	// =================================================
	
	private MyApplication mApp;
	private EditText etUsername;
	private EditText etPassword;
	private boolean isLoginInProccess = false;
	private TextView tvForgotPassword;
	private JSONObject jObj;
	private Bitmap myBitmap;
	private ImageView ivLearnLogo;
	private Button btnLogin;
	private String mErrorTitle;
	private String mErrorBody;
	
	// =================================================
	// OVERRIDDEN METHODS
	// =================================================
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_login);
		this.mApp = (MyApplication)getApplication();
		
		ivLearnLogo = (ImageView) findViewById(R.id.ivLogo);
		
		initViews();

		if (MyApplication.isInternetAvailable) {
			new GetImageTask().execute(getPropertiesString(PROP_ICON_URL));
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	
	@Override
	public boolean isNetworkConnected() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null) {
			// There are no active networks.
			return false;
		}
		else {
			return true;
		}
	}

	@Override
	public String getPropertiesString(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			return (String) mApp.mPropertiesSpecific.get(key);
		}
		else {
			return null;
		}
	}

	@Override
	public int getPropertiesInt(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			String tempString;
			tempString = (String) mApp.mPropertiesSpecific.get(key);
			tempString = tempString.replaceAll("\\s","");
			if (tempString == "" || tempString == null) {
				return 0;
			}
			else {
				return Integer.valueOf(tempString);
			}
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		// TODO Auto-generated method stub
		return null;
	}
	
	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	
	
	// =================================================
	// CLASS LOGIC
	// =================================================
	
	private void login(String username, String password) {

		MyApplication.pU = username;
		MyApplication.pP = password;

		if (!getPropertiesString(PROP_IDM_URL_PATH).equals(PLIST_NULL_VALUE)) {
			loginUsingIdm(username, password);
		}
		else {
			loginToMichlol(username, password);
		}
	}
	
	private void loginUsingIdm(final String username, final String password) {

		new MyIdmLoginRequest().newLogin(getPropertiesString(PROP_IDM_URL_PATH), getPropertiesString(PROP_MICHLOL_URL_PATH), new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						try{
							xmlParser xp = new xmlParser();
							InputStream stream = new ByteArrayInputStream(response.getBytes());

							IdmLoginData idmLoginData = xp.parseOutputDataNewLogin(new DataInputStream(stream));

							if (idmLoginData != null) {
								if (idmLoginData.retCode.equalsIgnoreCase(STR_SUCCESS)) {
									mApp.mIdmLoginData = idmLoginData;
									loginToMichlol(idmLoginData);
								}
								else {
									// value returned from IDM is different than success
									showErrorMessage(2);
								}
							}
							else {
								// connection timeout / error
								showErrorMessage(0);
							}
						}
						catch (Exception e){
							showErrorMessage(0);
						}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e("dfssd", error.toString());
						error.printStackTrace();
						showErrorMessage(0);

					}
				},username, password);
	}
	
	private void loginToMichlol(final String username, final String password) {

		MyMichlolRequests myMichlolLoginRequest = new MyMichlolRequests();

		myMichlolLoginRequest.call(
				new Response.Listener<String>() {
				   @Override
				   public void onResponse(String response) {
					   if (response != null) {
						   parseMichlolResponse(response);
						   checkLoginStatus();
					   }
					   else {
						   // connection timeout / error
						   showErrorMessage(1);
					   }
				   }
			   }, new Response.ErrorListener() {
				   @Override
				   public void onErrorResponse(VolleyError error) {
					   showErrorMessage(1);
				   }
			   }, REQ_LOGIN, username, password, myMichlolLoginRequest.getHebYear(), null, getPropertiesString(PROP_MICHLOL_URL_PATH), false);
	}
	
	private void loginToMichlol(final IdmLoginData idmLoginData)
	{
		MyMichlolRequests myMichlolLoginRequest = new MyMichlolRequests();


		if (getPropertiesInt(PROP_BRANCH_ID) == 4)
		{
			myMichlolLoginRequest.call(
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						if (response != null) {
							parseMichlolResponse(response);
							checkLoginStatus();
						}
						else {
							// connection timeout / error
							showErrorMessage(1);
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						showErrorMessage(1);
					}
				}, REQ_LOGIN, idmLoginData.tZehut, idmLoginData.tZehut, myMichlolLoginRequest.getHebYear(), null, getPropertiesString(PROP_MICHLOL_URL_PATH), false);
		}
		else
		{
			myMichlolLoginRequest.call(
					new Response.Listener<String>() {
						@Override
						public void onResponse(String response) {
							if (response != null) {
								parseMichlolResponse(response);
								checkLoginStatus();
							}
							else {
								// connection timeout / error
								showErrorMessage(1);
							}
						}
					}, new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							showErrorMessage(1);
						}
					}, REQ_LOGIN, idmLoginData.WorkforceID, idmLoginData.MichlolUserName, myMichlolLoginRequest.getHebYear(), null, getPropertiesString(PROP_MICHLOL_URL_PATH), false);
		}
	}
	
	private void parseMichlolResponse(String response) {
		mApp.mCurrentUserDetails = new UserDetailsData();
		xmlParser xp = new xmlParser();
		mApp.mCurrentUserDetails = xp.parseUserDetailsData(response);
		MyApplication.TOKEN = mApp.mCurrentUserDetails.TOKEN;
	}
	
	private void checkLoginStatus() {
		String currentLoginStatus = mApp.mCurrentUserDetails.LOGIN_STATUS;
		boolean isLoginStatusValid = true;
		if (currentLoginStatus != null) {
			if (!currentLoginStatus.equals("") && !currentLoginStatus.equals("1")) {
				String[] mLoginStatusArray = getPropertiesString(PROP_RESTRICTION_LOGIN_STATUS).split(",");
				for (String status : mLoginStatusArray) {
					if (currentLoginStatus.equalsIgnoreCase(status)) {
						isLoginStatusValid = false;
					}
				}
				if (isLoginStatusValid) {
					startMainMenuActivity();
				}
				else {
					showErrorMessage(1);
				}
			}else {
				showErrorMessage(2);
			}
		}
	}
	
	private void startMainMenuActivity() {
		if (!getPropertiesString(PROP_IDM_URL_PATH).equals(PLIST_NULL_VALUE)) {
			String username = "";
			if (getPropertiesInt(PROP_BRANCH_ID) == 4) {
				username = mApp.mIdmLoginData.tZehut;
			}
			else {
				username = mApp.mIdmLoginData.WorkforceID;
			}
			String password = mApp.mIdmLoginData.MichlolUserName;
			mApp.mPrefs.edit().putString(PREF_USER_NAME, username).commit();
			mApp.mPrefs.edit().putString(PREF_PASSWORD, password).commit();
		}
		else {
			mApp.mPrefs.edit().putString(PREF_USER_NAME, etUsername.getText().toString()).commit();
			mApp.mPrefs.edit().putString(PREF_PASSWORD, etPassword.getText().toString()).commit();
		}
		
		postUserInfo();
		Intent i = new Intent(this, MainMenu.class);
		startActivity(i);
		finish();
	}


	private void postUserInfo() {
		
		//PushManager mPushManager = PushManager.getInstance(getApplicationContext());
		//String apid = mPushManager.getDevicePushToken();
		
		jObj = new JSONObject();
		/*if (apid == null) {
			apid = "";
		}*/
		try {
			jObj.put("pass", mApp.mPrefs.getString(PREF_LOGIN_PASSWORD, ""));
			jObj.put("pid", mApp.mPrefs.getString(PREF_USER_NAME, ""));
			jObj.put("full_name", mApp.mCurrentUserDetails.FULLNAME);
			jObj.put("email", mApp.mCurrentUserDetails.STUDENT_EMAIL);
			jObj.put("birthday", "");
			jObj.put("phone", mApp.mCurrentUserDetails.STUDENT_CELLULARPHONE.replace("-", ""));
			jObj.put("gender", "");
			jObj.put("branch", getPropertiesString(PROP_BRANCH_ID));
			jObj.put("department", mApp.mCurrentUserDetails.STUDENT_DEPARTMENT);
			jObj.put("speciality", mApp.mCurrentUserDetails.STUDENT_SPECIALITY);
			jObj.put("current_year", mApp.mCurrentUserDetails.CURRENTYEAR);
			jObj.put("student_acamedic_year", mApp.mCurrentUserDetails.STUDENT_ACADEMIC_YEAR);
			jObj.put("address", mApp.mCurrentUserDetails.STUDENT_ADDRESS);
			jObj.put("teacher_id", mApp.mCurrentUserDetails.TEACHER_ID);
			jObj.put("status", mApp.mCurrentUserDetails.LOGIN_STATUS_TEXT);
			jObj.put("udid", android.provider.Settings.System.getString(super.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID));
			jObj.put("device_type", "Android");
			//jObj.put("device_token", apid);
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			jObj.put("version", pInfo.versionName);
			jObj.put("token", MyApplication.TOKEN);
			
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}


		/*Thread t = new Thread(new Runnable() {
			public void run() {
				String postUrl = getPropertiesString(PROP_POST_USER_INFO) + "student/register";
				//HttpClient httpclient = new DefaultHttpClient();
			    HttpPost httppost = new HttpPost(postUrl);
				HttpClient httpclient = createHttpClient();
			    try {
			        // Add your data
			    	StringEntity se = new StringEntity(jObj.toString(), HTTP.UTF_8);  
                    se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    httppost.setEntity(se);

			        // Execute HTTP Post Request
			        HttpResponse response = httpclient.execute(httppost);
			        

					// Get hold of the response entity
					HttpEntity entity = response.getEntity();
					// If the response does not enclose an entity, there is no need
					// to worry about connection release

					if (entity != null) {

						// A Simple JSON Response Read
						InputStream instream = entity.getContent();
						
						@SuppressWarnings("unused")
						String result = convertStreamToString(instream);
						
						// now you have the string representation of the HTML request
						instream.close();
					}
			        
			    } catch (ClientProtocolException e) {
			        // TODO Auto-generated catch block
			    } catch (IOException e) {
			        Log.e(TAG, e.toString());
			    }
			}
		});
		t.start();*/

		String postUrl = getPropertiesString(PROP_POST_USER_INFO) + "student/register";

		StringRequest req = new StringRequest(Request.Method.POST,
				postUrl,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {

					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {

					}
				}) {

			@Override
			public byte[] getBody() throws com.android.volley.AuthFailureError {
				try{
					return jObj.toString().getBytes();
				}
				catch (Exception e){
					return "".getBytes();
				}
			}

			@Override
			public String getBodyContentType()
			{
				return "application/json; charset=utf-8";
			}
		};

		MyApplication.getInstance().addToRequestQueue(req);
	}
	
	private void initViews() {

		//API KEY
		if(!MyApplication.pKeyParse(getPropertiesString(GlobalDefs.PROP_P_KEY))){
			Toast.makeText(getBaseContext(), getString(R.string.service_error), Toast.LENGTH_SHORT).show();

			finish();
			return;
		}

		TextView tvInstitute = (TextView) findViewById(R.id.login_tv_institute_name);
		tvInstitute.setText(getPropertiesString(PROP_INSTITUTE_NAME));
		TextView tvWelcome = (TextView) findViewById(R.id.login_tv_welcome_msg);
		tvWelcome.setText(getPropertiesString(PROP_WELCOME_MSG));
		etUsername = (EditText) findViewById(R.id.login_et_username);
		etPassword = (EditText) findViewById(R.id.login_et_password);
		btnLogin = (Button) findViewById(R.id.login_btn_login);
		
		tvForgotPassword = (TextView) findViewById(R.id.login_tv_forgot_password);
		tvForgotPassword.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent resetPassIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getPropertiesString(PROP_RESET_PASSWORD)));
				startActivity(resetPassIntent);
			}
		});
		
		etUsername.setText(mApp.mPrefs.getString(PREF_LOGIN_USER_NAME, ""));
		etPassword.setText(mApp.mPrefs.getString(PREF_LOGIN_PASSWORD, ""));
		
		btnLogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (!isLoginInProccess) {
					if (MyApplication.isInternetAvailable) {
						isLoginInProccess = true;
						if (etUsername.length() != 0 && etPassword.length() != 0) {
							setIsConnecting(true);
							 
							mApp.mPrefs.edit().putString(PREF_LOGIN_USER_NAME, etUsername.getText().toString()).commit();
							mApp.mPrefs.edit().putString(PREF_LOGIN_PASSWORD, etPassword.getText().toString()).commit();
				
							mApp.mPrefs.edit().putString(PREF_MICHLOL_URL, getPropertiesString(PROP_MICHLOL_URL_PATH)).commit();
							login(etUsername.getText().toString(), etPassword.getText().toString());
						}
						isLoginInProccess = false;
					}
					else {
						mApp.showNoInternetDialog(Login.this);
					}
				}
			}
		});
	}
	
	private void setIsConnecting(boolean isConnecting) {
		if (isConnecting) {
			btnLogin.setEnabled(false);
			btnLogin.setText(getResources().getString(R.string.my_loading));
		}
		else {
			btnLogin.setEnabled(true);
			btnLogin.setText(getResources().getString(R.string.login_login));
		}
	}
	
	private void showErrorMessage(int errorId) {
		switch (errorId) {
		case 0:
			// Getting no response from IDM server
			mErrorTitle = getString(R.string.my_conn_error_title);
			mErrorBody = getString(R.string.my_error_conn_problem);
			break;
		case 1:
			// Michlol status not allowed by pList
			if (mApp.mCurrentUserDetails != null) {
				if (mApp.mCurrentUserDetails.LOGIN_STATUS != null && mApp.mCurrentUserDetails.LOGIN_STATUS_TEXT != null) {
					mErrorTitle = getString(R.string.my_conn_error_title) + ": " + mApp.mCurrentUserDetails.LOGIN_STATUS;
					mErrorBody = mApp.mCurrentUserDetails.LOGIN_STATUS_TEXT;
				}
				else {
					mErrorTitle = getString(R.string.my_conn_error_title);
					mErrorBody = getString(R.string.my_error_conn_problem);
				}
			}
			else {
				mErrorTitle = getString(R.string.my_conn_error_title);
				mErrorBody = getString(R.string.my_error_conn_problem);
			}
			break;
		case 2:
			// IDM returns retCode ERROR
			mErrorTitle = getString(R.string.my_conn_error_title);
			mErrorBody = getString(R.string.my_error_idm_return_error);
			break;
		}
		this.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				setIsConnecting(false);

				AlertDialog alertDialog = new AlertDialog.Builder(Login.this).create();

				alertDialog.setTitle(mErrorTitle);

				alertDialog.setMessage(mErrorBody);

				alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
						getString(R.string.my_ok),
						new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

				try {
					alertDialog.show();
				} catch (Exception e) {
					// handle the exception
				}
			}
		});
	}
	
	// =================================================
	// GETTERS AND SETTERS
	// =================================================
	
	// =================================================
	// INNER CLASSES
	// =================================================
	
	private static String convertStreamToString(InputStream is) {
		/*
		 * To convert the InputStream to String we use the BufferedReader.readLine()
		 * method. We iterate until the BufferedReader return null which means
		 * there's no more data to read. Each line will appended to a StringBuilder
		 * and returned as String.
		 */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
	
	private class GetImageTask extends AsyncTask<String, Void, Void> {
		
		@Override
		protected Void doInBackground(String... arg0) {
			try {
		        java.net.URL url = new java.net.URL(arg0[0]);
		        HttpURLConnection connection = (HttpURLConnection) url
		                .openConnection();
		        connection.setDoInput(true);
		        connection.connect();
		        InputStream input = connection.getInputStream();
		        myBitmap = BitmapFactory.decodeStream(input);
		    } catch (IOException e) {
		        e.printStackTrace();
		        return null;
		    }
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			ivLearnLogo.setImageBitmap(myBitmap);
		}
	}
	
}
