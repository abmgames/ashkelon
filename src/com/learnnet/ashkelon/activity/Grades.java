package com.learnnet.ashkelon.activity;

import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Communication.MyMichlolRequests;
import com.learnnet.ashkelon.Fragments.GradeExpandFragment;
import com.learnnet.ashkelon.Fragments.GradesFragment;
import com.learnnet.ashkelon.Interface.MyInterface;
import com.learnnet.ashkelon.Objects.GradeData;
import com.learnnet.ashkelon.Objects.GradeDetailsData;
import com.learnnet.ashkelon.Utils.GlobalDefs;
import com.learnnet.ashkelon.Utils.xmlParser;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class Grades extends ActionBarActivity implements GlobalDefs, MyInterface, OnItemSelectedListener {

	// =================================================
	// FIELDS
	// =================================================

	private MyApplication mApp;
	private ActionBar mActionBar;
	private ProgressDialog spinnerDialog;
	private List<GradeData> mData;
	private List<GradeDetailsData> mGradeDetailsData;
	private TextView actionBarTitle;
	private GradeData mCurrectGradeData;
	private boolean isFirstFragment = true;

	// =================================================
	// OVERRIDDEN METHODS
	// =================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_grades);

		this.mApp = (MyApplication)getApplication();

		// Inflate your custom action bar layout
		final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.action_bar_centered_text, null);

		// Disable drawer swipe gesture 
		DrawerLayout mDrawerLayout;
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

		// Set up your ActionBar
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayShowCustomEnabled(true);
		mActionBar.setCustomView(actionBarLayout);

		// Enable ActionBar app icon to behave as action to toggle nav drawer
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayUseLogoEnabled(false);
		mActionBar.setDisplayHomeAsUpEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);

		// Set ActionBar's text
		actionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
		actionBarTitle.setText(getResources().getString(R.string.my_grades_title));

		if (savedInstanceState == null) {
			selectItem(0);
		}

	}

	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_inner, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean isNetworkConnected() {
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			return (String) mApp.mPropertiesSpecific.get(key);
		}
		else {
			return null;
		}
	}

	@Override
	public int getPropertiesInt(String key) {
		return 0;
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}
	
	@Override
	public void onBackPressed() {
		FragmentManager fragmentManager = getSupportFragmentManager();
		if (fragmentManager.getBackStackEntryCount() > 0) {
			fragmentManager.popBackStack();
			actionBarTitle.setText(getResources().getString(R.string.my_grades_title));
		}
		else {
			super.onBackPressed();
		}
	}

	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================

	// =================================================
	// CLASS LOGIC
	// =================================================

	private void selectItem(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (position) {
		case 0:
			fragment = new GradesFragment();
			actionBarTitle.setText(getResources().getString(R.string.my_grades_title));
			break;
		case 1:
			fragment = new GradeExpandFragment();
			break;
		default:
			fragment = new GradesFragment();
			break;
		}
		if (fragment != null) {

			FragmentManager fragmentManager = getSupportFragmentManager();
			if (isFirstFragment) {
				isFirstFragment = false;
				fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			}
			else {
				fragmentManager.beginTransaction().add(R.id.content_frame, fragment).addToBackStack(null).commit();
			}
		}
	}

	public void displaySpinner() {
		spinnerDialog = ProgressDialog.show(Grades.this, "",
				getString(R.string.my_wait), true);
	}

	public void hideSpinner() {
		spinnerDialog.cancel();
	}
	
	public void getGradeDetails(GradeData gradeData) {
		mCurrectGradeData = gradeData;
		//new GetGradeDetailsTask().execute(gradeData.courseId);

		String username = mApp.mPrefs.getString(PREF_USER_NAME, "");
		String url = mApp.mPrefs.getString(PREF_MICHLOL_URL, "");

		displaySpinner();

		MyMichlolRequests michlolMessagesRequest = new MyMichlolRequests();

		michlolMessagesRequest.call(
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						xmlParser xp = new xmlParser();
						mGradeDetailsData = xp.parseGradeDetailsData(response);

						hideSpinner();
						setGradeDetailsData(mGradeDetailsData);
						openGrade();
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						hideSpinner();
					}
				},
			REQ_GRADE_DETAILS, username, "UnsedValueJustForChecks", gradeData.courseId, null, url, false);
	}
	
	private void openGrade() {
		actionBarTitle.setText(mCurrectGradeData.courseName);
		selectItem(1);
	}
	
	/*private String michlolRequestGradeDetails(String userName, String url, String courseId) {
		MyMichlolRequests michlolMessagesRequest = new MyMichlolRequests();
		return michlolMessagesRequest.call(REQ_GRADE_DETAILS, userName, "UnsedValueJustForChecks",
				courseId, null, url, false); // false doesn't matter here.
	}*/

	// =================================================
	// GETTERS AND SETTERS
	// =================================================

	public MyApplication getMyApp() {
		return this.mApp;
	}

	public List<GradeData> getGradesData() {
		return this.mData;
	}
	
	public void setGradesData(List<GradeData> data) {
		this.mData = data;
	}
	
	public List<GradeDetailsData> getGradeDetailsData() {
		return this.mGradeDetailsData;
	}
	
	public void setGradeDetailsData(List<GradeDetailsData> details) {
		this.mGradeDetailsData = details;
	}
	
	public GradeData getCurrectGradeData() {
		return mCurrectGradeData;
	}

	public void setCurrectGradeData(GradeData currectGradeData) {
		this.mCurrectGradeData = currectGradeData;
	}

	// =================================================
	// INNER CLASSES
	// =================================================

}
