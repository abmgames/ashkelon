package com.learnnet.ashkelon.Adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Objects.GradeDetailsData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class GradeDetailsAdapter extends ArrayAdapter<GradeDetailsData> {

	public Activity activity;
	public List<GradeDetailsData> tweets;

	public GradeDetailsAdapter(Activity a, int textViewResourceId, List<GradeDetailsData> tweets) {
		super(a, textViewResourceId, tweets);
		this.tweets = tweets;
		activity = a;
	}

	public static class ViewHolder {
		public TextView text1;
		public TextView text2;
		public TextView text3;
		public TextView text4;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;
		ViewHolder holder;

		final GradeDetailsData tweet = tweets.get(position);

		if (v == null) {

			LayoutInflater vi = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.grade_details_list_item, null);
			holder = new ViewHolder();
			holder.text1 = (TextView) v.findViewById(R.id.line1);
			holder.text2 = (TextView) v.findViewById(R.id.line2);
			holder.text3 = (TextView) v.findViewById(R.id.line3);
			holder.text4 = (TextView) v.findViewById(R.id.line4);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		if (tweet != null) {

//			holder.text1.setText(tweet.DTUPDZIN.substring(0, tweet.DTUPDZIN.indexOf(" ")));
			holder.text1.setText(tweet.name);
			holder.text2.setText(tweet.semester);
			holder.text3.setText(tweet.weight);
			holder.text4.setText(tweet.grade);
		}

		return v;
	}

}
