package com.learnnet.ashkelon.Fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Adapters.MyAcademicMenuListViewAdapter;
import com.learnnet.ashkelon.Extensions.MyAcademicMenuManager;
import com.learnnet.ashkelon.Interface.MyInterface;
import com.learnnet.ashkelon.Utils.GlobalDefs;
import com.learnnet.ashkelon.activity.MainMenu;
import com.learnnet.ashkelon.activity.Messages;
import com.learnnet.ashkelon.activity.OnlineEducation;
import com.learnnet.ashkelon.activity.Schedule;
import com.learnnet.ashkelon.activity.TeacherExams;
import com.learnnet.ashkelon.activity.TeacherLessons;
import com.learnnet.ashkelon.activity.TeacherSchedule;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0,0
 * @since       2014-03-14
 */
public class AcademicTeacherMenuFragment extends Fragment implements GlobalDefs, OnItemClickListener, MyInterface {
	
	// =================================================
	// FIELDS
	// =================================================
	
	private ArrayList<Map<String, String>> teacherTableFeaturesArray;
	private MyApplication mApp;
	
	// =================================================
	// CONSTRUCTORS / SINGLETON
	// =================================================
	
	// =================================================
	// OVERRIDDEN METHODS
	// =================================================
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_academic_menu, container, false);
		
		mApp = ((MainMenu)getActivity()).getMyApp();
	
		MyAcademicMenuManager mamm = new MyAcademicMenuManager(getPropertiesInt(PROP_BRANCH_ID), ((MainMenu)getActivity()).getIsTeacher());
		teacherTableFeaturesArray = mamm.getMenu();
		
		ArrayList<Map<String, String>> data = new ArrayList<Map<String,String>>(teacherTableFeaturesArray);
		
		ListView mAcademicList = (ListView) v.findViewById(R.id.lv_academic_menu);
		mAcademicList.setAdapter(new MyAcademicMenuListViewAdapter(((MainMenu)getActivity()), data));
		mAcademicList.setOnItemClickListener(this);
		
		return v;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Intent i;
		switch (arg2) { // arg2 stands for list position
			case 0:
				// Schedule
				if (MyApplication.isInternetAvailable) {
					i = new Intent((MainMenu)getActivity(), Messages.class);
					startActivity(i);
				}
				else {
					mApp.showNoInternetDialog((MainMenu)getActivity());
				}
				break;
			case 1:
				// Schedule
				if (MyApplication.isInternetAvailable) {
					i = new Intent((MainMenu)getActivity(), TeacherSchedule.class);
					startActivity(i);
				}
				else {
					mApp.showNoInternetDialog((MainMenu)getActivity());
				}
				break;
		case 2:
			// Exams
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), TeacherExams.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		case 3:
			// Online education
			
//			Old version:
//			MyInstituteUrlManager mium = new MyInstituteUrlManager(getPropertiesInt(PROP_BRANCH_ID));
//			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mium.onlineEducationUrl));
//			startActivity(browserIntent);
			
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), OnlineEducation.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			
			break;
		case 4:
			// Schedule
			if (MyApplication.isInternetAvailable) {
				i = new Intent((MainMenu)getActivity(), TeacherLessons.class);
				startActivity(i);
			}
			else {
				mApp.showNoInternetDialog((MainMenu)getActivity());
			}
			break;
		default:
			break;
		}
	}

	@Override
	public boolean isNetworkConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		MyApplication mApp = ((MainMenu)getActivity()).getMyApp();
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			String tempString;
			tempString = (String) mApp.mPropertiesSpecific.get(key);
			tempString = tempString.replaceAll("\\s","");
			if (tempString == "" || tempString == null) {
				return 0;
			}
			else {
				return Integer.valueOf(tempString);
			}
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		// TODO Auto-generated method stub
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			Map<String, Object> map = new HashMap<String, Object>();
			map = (Map<String, Object>) mApp.mPropertiesSpecific.get(key);
			return map;
		}
		else {
			return null;
		}
	}
	
	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	
	
	// =================================================
	// CLASS LOGIC
	// =================================================
	
	// =================================================
	// GETTERS AND SETTERS
	// =================================================
	
	// =================================================
	// INNER CLASSES
	// =================================================

}