package com.learnnet.ashkelon.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.ViewGroup;
import android.widget.TextView;

import com.learnnet.ashkelon.R;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class InboxExpand extends ActionBarActivity {

	// =================================================
	// FIELDS
	// =================================================
	
	private String mTitle;
	private String mDate;
	private String mBody;
	private TextView mDateTextView;
	private TextView mBodyTextView;
	private ActionBar mActionBar;
	private TextView actionBarTitle;

	// =================================================
	// OVERRIDDEN METHODS
	// =================================================
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_inbox_expand);
	
		mDateTextView = (TextView) findViewById(R.id.tvDate);
		mBodyTextView = (TextView) findViewById(R.id.tvBody);
		
		mTitle = getIntent().getStringExtra("title");
		mDate = getIntent().getStringExtra("date");
		mBody = getIntent().getStringExtra("body");
		
		mDateTextView.setText(mDate);
		mBodyTextView.setText(mBody);
		
		initializeActionBar();
	}
	
	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================
	
	// =================================================
	// CLASS LOGIC
	// =================================================
	
	private void initializeActionBar() {

		// Inflate your custom action bar layout
		final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.action_bar_centered_text, null);

		// Set up your ActionBar
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayShowCustomEnabled(true);
		mActionBar.setCustomView(actionBarLayout);

		// Set up your ActionBar
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayUseLogoEnabled(false);
		mActionBar.setDisplayHomeAsUpEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);

		// Set ActionBar's text
		actionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
		actionBarTitle.setText(mTitle);
	}
	
	// =================================================
	// GETTERS AND SETTERS
	// =================================================
	
	// =================================================
	// INNER CLASSES
	// =================================================
	
}
