package com.learnnet.ashkelon.activity;

import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Communication.MyMichlolRequests;
import com.learnnet.ashkelon.Fragments.RequestsFragment;
import com.learnnet.ashkelon.Interface.MyInterface;
import com.learnnet.ashkelon.Objects.RequestData;
import com.learnnet.ashkelon.Utils.GlobalDefs;
import com.learnnet.ashkelon.Utils.xmlParser;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class Requests extends ActionBarActivity implements GlobalDefs, MyInterface {

	// =================================================
		// FIELDS
		// =================================================

		private MyApplication mApp;
		private ActionBar mActionBar;
		private ProgressDialog spinnerDialog;
		private List<RequestData> mData;
		private TextView actionBarTitle;

		// =================================================
		// OVERRIDDEN METHODS
		// =================================================

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_requests);

			this.mApp = (MyApplication)getApplication();

			// Inflate your custom action bar layout
			final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
					R.layout.action_bar_centered_text, null);

			// Disable drawer swipe gesture 
			DrawerLayout mDrawerLayout;
			mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
			mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

			// Set up your ActionBar
			mActionBar = getSupportActionBar();
			mActionBar.setDisplayShowCustomEnabled(true);
			mActionBar.setCustomView(actionBarLayout);

			// Enable ActionBar app icon to behave as action to toggle nav drawer
			mActionBar.setDisplayShowHomeEnabled(false);
			mActionBar.setDisplayUseLogoEnabled(false);
			mActionBar.setDisplayHomeAsUpEnabled(false);
			mActionBar.setDisplayShowTitleEnabled(false);

			// Set ActionBar's text
			actionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
			actionBarTitle.setText(getResources().getString(R.string.my_requests_title));

			if (savedInstanceState == null) {
				//new GetRequestsTask().execute();
				displaySpinner();

				String username = mApp.mPrefs.getString(PREF_USER_NAME, "");
				String url = mApp.mPrefs.getString(PREF_MICHLOL_URL, "");

				MyMichlolRequests michlolRequestsRequest = new MyMichlolRequests();
				michlolRequestsRequest.call(
					new Response.Listener<String>() {
						@Override
						public void onResponse(String response) {
							xmlParser xp = new xmlParser();
							mData = xp.parseRequestsData(response);

							hideSpinner();
							selectItem(0);
						}
					},
					new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							hideSpinner();
						}
					},
				REQ_REQUESTS, username, "UnsedValueJustForChecks", null, null, url, false); // false doesn't matter here.
			}

		}
		
		@Override
		protected void onStart() {
			super.onStart();
			GoogleAnalytics.getInstance(this).reportActivityStart(this);
		}

		@Override
		protected void onStop() {
			super.onStop();
			GoogleAnalytics.getInstance(this).reportActivityStop(this);
		}

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.menu_inner, menu);
			return super.onCreateOptionsMenu(menu);
		}

		@Override
		public boolean isNetworkConnected() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public String getPropertiesString(String key) {
			if (mApp.mPropertiesSpecific.containsKey(key)) {
				return (String) mApp.mPropertiesSpecific.get(key);
			}
			else {
				return null;
			}
		}

		@Override
		public int getPropertiesInt(String key) {
			return 0;
		}

		@Override
		public boolean getPropertiesBoolean(String key) {
			return false;
		}

		@Override
		public Map<String, Object> getPropertiesHashTable(String key) {
			return null;
		}

		// =================================================
		// STATIC VARIABLES AND METHODS
		// =================================================

		// =================================================
		// CLASS LOGIC
		// =================================================

		private void selectItem(int position) {
			// update the main content by replacing fragments
			Fragment fragment = null;
			switch (position) {
			case 0:
				fragment = new RequestsFragment();
				actionBarTitle.setText(getResources().getString(R.string.my_requests_title));
				break;
			default:
				fragment = new RequestsFragment();
				break;
			}
			if (fragment != null) {

				FragmentManager fragmentManager = getSupportFragmentManager();
				fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			}
		}

		public void displaySpinner() {
			spinnerDialog = ProgressDialog.show(this, "",
					getString(R.string.my_wait), true);
		}

		public void hideSpinner() {
			spinnerDialog.cancel();
		}

		/*private String michlolRequestRequests(String username, String url) {
			MyMichlolRequests michlolRequestsRequest = new MyMichlolRequests();
			return michlolRequestsRequest.call(REQ_REQUESTS, username, "UnsedValueJustForChecks",
					null, null, url, false); // false doesn't matter here.
		}*/

		// =================================================
		// GETTERS AND SETTERS
		// =================================================

		public MyApplication getMyApp() {
			return this.mApp;
		}

		public List<RequestData> getRequestsData() {
			return this.mData;
		}

}
