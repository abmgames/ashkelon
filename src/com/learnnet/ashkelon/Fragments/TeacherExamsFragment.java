package com.learnnet.ashkelon.Fragments;

import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Adapters.TeacherExamsAdapter;
import com.learnnet.ashkelon.Interface.MyInterface;
import com.learnnet.ashkelon.Objects.TeacherExamData;
import com.learnnet.ashkelon.Utils.GlobalDefs;
import com.learnnet.ashkelon.activity.TeacherExams;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class TeacherExamsFragment extends Fragment implements GlobalDefs, MyInterface {
	
	private ListView mExamsListView;
	private TeacherExamsAdapter mTeacherAdapter;
	private List<TeacherExamData> mData;
	private MyApplication mApp;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.fragment_teacher_exams, container, false);
		
		mExamsListView = (ListView) v.findViewById(R.id.lv_teacher_exams);
		mApp = ((TeacherExams)getActivity()).getMyApp();
		
		mData = ((TeacherExams)getActivity()).getTeacherExamsData();
		
		mTeacherAdapter = new TeacherExamsAdapter((TeacherExams)getActivity(), R.layout.teacher_exam_list_item, mData);
		mExamsListView.setAdapter(mTeacherAdapter);

		return v;
	}

	@Override
	public boolean isNetworkConnected() {
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			String tempString;
			tempString = (String) mApp.mPropertiesSpecific.get(key);
			tempString = tempString.replaceAll("\\s","");
			if (tempString == "" || tempString == null) {
				return 0;
			}
			else {
				return Integer.valueOf(tempString);
			}
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}
}
