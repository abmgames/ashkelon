package com.learnnet.ashkelon.Fragments;

import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Interface.MyInterface;
import com.learnnet.ashkelon.Utils.GlobalDefs;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class ExamExpandFragment extends Fragment implements MyInterface, GlobalDefs {

	// =================================================
	// FIELDS
	// =================================================

	private TextView mTitleTextView;
	private TextView mDateTextView;
	private TextView mTimeTextView;
	private TextView mLecturerTextView;
	private TextView mClassRoomTextView;
	
	// =================================================
	// OVERRIDDEN METHODS
	// =================================================

	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================	

	// =================================================
	// CLASS LOGIC
	// =================================================

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Bundle b = getArguments();
		String mTitle = "";
		String mDate = "";
		String mTime = "";
		String mLecturer = "";
		String mClassRoom = "";
		if (b != null) {
			mTitle = b.getString(BUNDLE_EXAM_TITLE);
			mDate = b.getString(BUNDLE_EXAM_DATE);
			mTime = b.getString(BUNDLE_EXAM_TIME);
			mLecturer = b.getString(BUNDLE_EXAM_LECTURER);
			mClassRoom = b.getString(BUNDLE_EXAM_CLASS);
		}
		View v = (View) inflater.inflate(R.layout.fragment_exam_expand, container, false);
		mTitleTextView = (TextView) v.findViewById(R.id.tvExamTitle);
		mDateTextView = (TextView) v.findViewById(R.id.tvExamDate);
		mTimeTextView = (TextView) v.findViewById(R.id.tvExamTime);
		mLecturerTextView = (TextView) v.findViewById(R.id.tvExamLecturer);
		mClassRoomTextView = (TextView) v.findViewById(R.id.tvExamClassRoom);
		mTitleTextView.setText(mTitle);
		mDateTextView.setText(mDate);
		mTimeTextView.setText(mTime);
		mLecturerTextView.setText(mLecturer);
		mClassRoomTextView.setText(mClassRoom);
		return v;
	}

	@Override
	public boolean isNetworkConnected() {
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		return null;
	}

	@Override
	public int getPropertiesInt(String key) {
		return 0;
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}

	// =================================================
	// GETTERS AND SETTERS
	// =================================================

	// =================================================
	// INNER CLASSES
	// =================================================
	
}
