package com.learnnet.ashkelon.Handlers;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.learnnet.ashkelon.Objects.RequestData;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       01/07/2014
 */
public class RequestsHandler extends DefaultHandler {

	List<RequestData> requests;
	private RequestData currentRequest;
	
	private StringBuilder builder;
	
	public List<RequestData> getMessages() {
        return this.requests;
    }
	
	@Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        super.characters(ch, start, length);
        builder.append(ch, start, length);
    }
	
	@Override
    public void endElement(String uri, String localName, String name)
            throws SAXException {
        super.endElement(uri, localName, name);
        
            if (localName.equalsIgnoreCase("REQUESTNAME"))
            {
            	currentRequest.name = builder.toString();
            }
            else if (localName.equalsIgnoreCase("REQUESTDATE"))
            {
            	currentRequest.date = builder.toString();
            } 
            else if (localName.equalsIgnoreCase("REQUESTSUBJECT"))
            {
            	currentRequest.subject = builder.toString();
            }
            else if (localName.equalsIgnoreCase("RQUESTSTATUS"))
            {
            	currentRequest.status = builder.toString();
            }
            else if (localName.equalsIgnoreCase("REQUESTDATEANSWERED"))
            {
            	currentRequest.dateAnswered = builder.toString();
            }
            else if (localName.equalsIgnoreCase("REQUESTTOPIC"))
            {
            	currentRequest.topic = builder.toString();
            }
            else if (localName.equalsIgnoreCase("REQUESTBODY"))
            {
            	currentRequest.body = builder.toString();
            }
            else if (localName.equalsIgnoreCase("REQUESTREPLYBODY"))
            {
            	currentRequest.replyBody = builder.toString();
            }
            else if (localName.equalsIgnoreCase("RECORD"))
            {
            	requests.add(currentRequest);
            }
            builder.setLength(0);    
        
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        requests = new ArrayList<RequestData>();
        builder = new StringBuilder();
    }

    @Override
    public void startElement(String uri, String localName, String name,
            Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (localName.equalsIgnoreCase("RECORD")){
            this.currentRequest = new RequestData();
        }
    }
}
