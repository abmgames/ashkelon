package com.learnnet.ashkelon.Extensions;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     2.0.0
 * @since       2014-05-09
 */
public class MyInstituteUrlManager {
	
	public String onlineEducationUrl;
	public String onlineEducationUrlWithoutLogin;
	
	public MyInstituteUrlManager(int instituteBranchID) {
		switch (instituteBranchID) {

		case 1:
			onlineEducationUrl = "http://moodle.telhai.ac.il/";
			onlineEducationUrlWithoutLogin = "http://moodle.telhai.ac.il/";
			break;
		case 2:
			onlineEducationUrl = "http://moodle.ash-college.ac.il/login/index.php";
			onlineEducationUrlWithoutLogin = "http://moodle.ash-college.ac.il/login/index.php";
			break;
		case 3:
			onlineEducationUrl = "http://moodle.ruppin.ac.il/login/index.php";
			onlineEducationUrlWithoutLogin = "http://moodle.ruppin.ac.il/";
			break;
		case 4:
			onlineEducationUrl = "http://md.hit.ac.il/";
			onlineEducationUrlWithoutLogin = "http://md.hit.ac.il/";
			break;
		case 5:
			onlineEducationUrl = "http://moodle.ruppin.ac.il/login/index.php";
			onlineEducationUrlWithoutLogin = "http://moodle.ruppin.ac.il/";
			break;
		case 6:
			onlineEducationUrl = "http://moodle.ruppin.ac.il/login/index.php";
			onlineEducationUrlWithoutLogin = "http://moodle.ruppin.ac.il/";
			break;
		case 7:
			onlineEducationUrl = "http://online.hemdat.ac.il/login/index.php";
			onlineEducationUrlWithoutLogin = "http://online.hemdat.ac.il/";
			break;
		case 8:
			onlineEducationUrl = "http://online.shenkar.ac.il/moodle2/login/index.php";
			onlineEducationUrlWithoutLogin = "http://online.shenkar.ac.il/moodle2/";
			break;
		case 9:
			onlineEducationUrl = "http://moodle.ruppin.ac.il/login/index.php";
			onlineEducationUrlWithoutLogin = "http://moodle.ruppin.ac.il/";
			break;
		case 10:
			onlineEducationUrl = "http://moodle.ruppin.ac.il/login/index.php";
			onlineEducationUrlWithoutLogin = "http://moodle.ruppin.ac.il/";
			break;
		default:
			onlineEducationUrl = "http://moodle.ruppin.ac.il/login/index.php";
			onlineEducationUrlWithoutLogin = "http://moodle.ruppin.ac.il/";
			break;
		}
	}
}
