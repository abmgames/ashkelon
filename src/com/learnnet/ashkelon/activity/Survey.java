package com.learnnet.ashkelon.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.learnnet.ashkelon.Communication.*;
import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Fragments.SurveyFragment;
import com.learnnet.ashkelon.Interface.MyInterface;
import com.learnnet.ashkelon.Utils.GlobalDefs;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class Survey extends ActionBarActivity implements GlobalDefs, MyInterface, OnItemSelectedListener {

	// =================================================
	// FIELDS
	// =================================================

	private MyApplication mApp;
	private Fragment mCurrentFragment;
	private ActionBar mActionBar;
	private TextView actionBarTitle;
	private String url;

	// =================================================
	// OVERRIDDEN METHODS
	// =================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_survey);

		this.mApp = (MyApplication)getApplication();

		// Inflate your custom action bar layout
		final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater().inflate(
				R.layout.action_bar_centered_text, null);

		// Disable drawer swipe gesture 
		DrawerLayout mDrawerLayout;
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

		// Set up your ActionBar
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayShowCustomEnabled(true);
		mActionBar.setCustomView(actionBarLayout);

		// Enable ActionBar app icon to behave as action to toggle nav drawer
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayUseLogoEnabled(false);
		mActionBar.setDisplayHomeAsUpEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);

		// Set ActionBar's text
		actionBarTitle = (TextView) findViewById(R.id.tvActionBarTitle);
		actionBarTitle.setText(getResources().getString(R.string.my_survey_title));

		// Set survey URL
		url = getPropertiesString(PROP_FEED_BACK);
		String id = mApp.mPrefs.getString(PREF_USER_NAME, "");
		String branch = getPropertiesString(PROP_BRANCH_ID);
		url += "student/feedback?branch=" + branch + "&student_id=" + id;
		//new GetUrlTask().execute(url);

		com.learnnet.ashkelon.Communication.Requests.string(Request.Method.GET, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				url = response;
				checkResponse();
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {

			}
		});
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_inner, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean isNetworkConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getPropertiesString(String key) {
		if (mApp.mPropertiesSpecific.containsKey(key)) {
			return (String) mApp.mPropertiesSpecific.get(key);
		}
		else {
			return null;
		}
	}

	@Override
	public int getPropertiesInt(String key) {
		return 0;
	}

	@Override
	public boolean getPropertiesBoolean(String key) {
		return false;
	}

	@Override
	public Map<String, Object> getPropertiesHashTable(String key) {
		return null;
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBackPressed() {
		if (mCurrentFragment != null) {
			WebView wv = ((SurveyFragment)mCurrentFragment).getWebView();
			if (wv.canGoBack()) {
				wv.goBack();
			}
			else {
				super.onBackPressed();
			}
		}
		else {
			super.onBackPressed();
		}
	}

	// =================================================
	// STATIC VARIABLES AND METHODS
	// =================================================

	// =================================================
	// CLASS LOGIC
	// =================================================

	private void selectItem(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		fragment = new SurveyFragment();
		Bundle args = new Bundle();
		args.putString(BUNDLE_URL, url);
		fragment.setArguments(args);

		if (fragment != null) {

			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			mCurrentFragment = fragment;
		}
	}

	private void checkResponse() {
		// Check URL response from web request
		try {
			JSONObject jObject = new JSONObject(url);
			int status = jObject.getInt(SURVEY_STATUS);
			if (status == SURVEY_STATUS_OK) {
				url = jObject.getString(SURVEY_URL);
				selectItem(0);
			}
			else {
				url = "";
			}
		}
		catch (JSONException e) {
			//Log.e(TAG + " Json Exception", e.toString());
		}
	}

	// =================================================
	// GETTERS AND SETTERS
	// =================================================

	public MyApplication getMyApp() {
		return this.mApp;
	}

	// =================================================
	// INNER CLASSES
	// =================================================

	private static String convertStreamToString(InputStream is) {
		/*
		 * To convert the InputStream to String we use the BufferedReader.readLine()
		 * method. We iterate until the BufferedReader return null which means
		 * there's no more data to read. Each line will appended to a StringBuilder
		 * and returned as String.
		 */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	/*private class GetUrlTask extends AsyncTask<String, Void, Void> {

		private HttpClient createHttpClient() {
			try {
				KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
				trustStore.load(null, null);

				SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
				sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

				HttpParams params = new BasicHttpParams();
				HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
				HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

				SchemeRegistry registry = new SchemeRegistry();
				registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
				registry.register(new Scheme("https", sf, 443));

				ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

				return new DefaultHttpClient(ccm, params);
			} catch (Exception e) {
				return new DefaultHttpClient();
			}
		}

		@Override
		protected Void doInBackground(String... arg0) {
			try {

				HttpClient httpclient = new DefaultHttpClient();

				// Prepare a request object
				HttpGet httpget = new HttpGet(arg0[0]); // arg0[0] - feedback url 

				// Execute the request
				HttpResponse response;

				response = httpclient.execute(httpget);

				// Get hold of the response entity
				HttpEntity entity = response.getEntity();
				// If the response does not enclose an entity, there is no need
				// to worry about connection release

				if (entity != null) {

					// A Simple JSON Response Read
					InputStream instream = entity.getContent();
					String result= convertStreamToString(instream);
					// now you have the string representation of the HTML request
					instream.close();

					url = result;				
				}


			} catch (Exception e) {
				Log.e(TAG + " Exception", e.toString());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			checkResponse();
		}
	}*/
}
