package com.learnnet.ashkelon.Objects;

/**
 * @author      Michael Groenendijk   Michael.groenendijk1@gmail.com
 * @version     1.0,0
 * @since       2013-11-10
 */
public class UserDetailsData {
	public String FULLNAME;
	public String STUDENT_ACADEMIC_YEAR;
	public String LOGIN_STATUS;
	public String LOGIN_STATUS_TEXT;
	public String STUDENT_SPECIALITY;
	public String STUDENT_EMAIL;
	public String STUDENT_STATUS;
	public String STUDENT_DEPARTMENT;
	public String TEACHER_ID;
	public String CURRENTYEAR;
	public String STUDENT_CELLULARPHONE;
	public String STUDENT_ADDRESS;
	public String TOKEN;

	public UserDetailsData() {
		FULLNAME = "";
		STUDENT_ACADEMIC_YEAR = "";
		LOGIN_STATUS = "";
		LOGIN_STATUS_TEXT = "";
		STUDENT_SPECIALITY = "";
		STUDENT_EMAIL = "";
		STUDENT_STATUS = "";
		STUDENT_DEPARTMENT = "";
		TEACHER_ID = "";
		CURRENTYEAR = "";
		STUDENT_CELLULARPHONE = "";
		STUDENT_ADDRESS = "";
		TOKEN = "";
	}

}

