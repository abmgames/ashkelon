package com.learnnet.ashkelon.Fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.learnnet.ashkelon.Adapters.GradesAdapter;
import com.learnnet.ashkelon.Adapters.PresenceAdapter;
import com.learnnet.ashkelon.Adapters.StudentsAdapter;
import com.learnnet.ashkelon.Communication.MyMichlolRequests;
import com.learnnet.ashkelon.Interface.MyInterface;
import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.Objects.GradeData;
import com.learnnet.ashkelon.Objects.StudentData;
import com.learnnet.ashkelon.R;
import com.learnnet.ashkelon.Utils.GlobalDefs;
import com.learnnet.ashkelon.Utils.xmlParser;
import com.learnnet.ashkelon.activity.Grades;
import com.learnnet.ashkelon.activity.ListPresenceActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Yoni on 02/02/2016.
 */
public class ListPresenceFragment extends Fragment {

    // =================================================
    // FIELDS
    // =================================================

    private ListView mPresenceListView;
    private PresenceAdapter mPresenceAdapter;
    private LinkedList<StudentData> mData;
    private MyApplication mApp;
    private SpinnerAdapter mSpinnerAdapter;
    private ArrayList<String> studentYears;

    // =================================================
    // CONSTRUCTORS / SINGLETON
    // =================================================

    // =================================================
    // OVERRIDDEN METHODS
    // =================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        /*View v = inflater.inflate(R.layout.fragment_list_presence, container, false);

        mPresenceListView = (ListView) v.findViewById(R.id.presence_list);

        mApp = ((ListPresenceActivity)getActivity()).getMyApp();

        mData = ((ListPresenceActivity)getActivity()).getListPresenceData();

        //mPresenceListView.setOnItemClickListener(this);

        studentYears = new ArrayList<String>();
        for (int i=0 ; i<mApp.mCurrentUserYears.size() ; i++) {
            studentYears.add(mApp.mCurrentUserYears.get(i).getYear());
        }

        showList();

        return v;*/
        return null;
    }

    private void showList() {

        StudentsAdapter adapter = new StudentsAdapter(getActivity(), mData);
        mPresenceListView.setAdapter(adapter);
    }

}