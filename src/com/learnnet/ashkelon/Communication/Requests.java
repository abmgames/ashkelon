package com.learnnet.ashkelon.Communication;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.StringRequest;
import com.learnnet.ashkelon.MyApplication;

import java.util.HashMap;
import java.util.Map;

public class Requests {

    public static void string(int method, final String url, Response.Listener<String> respListener, Response.ErrorListener errorListener)
    {
        StringRequest req = new StringRequest(method,
                url,
                respListener,
                errorListener);

        MyApplication.getInstance().addToRequestQueue(req);
    }

}
