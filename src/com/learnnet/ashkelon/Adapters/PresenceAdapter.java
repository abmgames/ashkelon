package com.learnnet.ashkelon.Adapters;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.learnnet.ashkelon.MyApplication;
import com.learnnet.ashkelon.Objects.GradeData;
import com.learnnet.ashkelon.Objects.StudentData;
import com.learnnet.ashkelon.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Yoni on 02/02/2016.
 */
public class PresenceAdapter extends ArrayAdapter<StudentData> {

    public Activity activity;
    public List<StudentData> tweets;
    private MyApplication mApp;
    private int rowHeight = 158;
    private int rowWidth;
    private float studentNameTextSize, studentInfoTextHeight;
    private int vIconSize;

    private HashMap<String, View> viewCache = new HashMap<String, View>();
    private HashMap<String, Integer> studentsStatus = new HashMap<String, Integer>();

    public PresenceAdapter(Activity a, int textViewResourceId, List<StudentData> tweets) {
        super(a.getApplicationContext(), textViewResourceId, tweets);
        this.tweets = tweets;
        activity = a;

        mApp = (MyApplication) activity.getApplication();

        try{
            DisplayMetrics metrics = new DisplayMetrics();
            a.getWindowManager().getDefaultDisplay().getMetrics(metrics);

            rowHeight = (int)(metrics.heightPixels * 0.09);
            rowWidth = metrics.widthPixels;

            studentInfoTextHeight = pixelsToSp(rowHeight * 0.5f * 0.37f);
            studentNameTextSize = pixelsToSp(rowHeight * 0.5f * 0.57f);
            vIconSize = (int)(rowHeight * 0.5f);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public class ViewHolder {
        public LinearLayout container;
        public ImageView studentImage;
        public TextView studentName;
        public TextView studentCourse;
        public ImageView vIcon;
        public LinearLayout buttonsContainer;
        public TextView absenceButton;
        public TextView lateButton;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null){
            Log.e("yoni", position + " convertView == null");

            LayoutInflater inflater = activity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.presence_list_item, null);

            holder = new ViewHolder();

            holder.container  = (LinearLayout) convertView.findViewById(R.id.container);
            holder.studentImage  = (ImageView) convertView.findViewById(R.id.student_image);
            holder.studentName   = (TextView) convertView.findViewById(R.id.student_name);
            holder.studentCourse = (TextView) convertView.findViewById(R.id.student_course);
            holder.vIcon = (ImageView) convertView.findViewById(R.id.v_icon);
            holder.buttonsContainer = (LinearLayout) convertView.findViewById(R.id.buttons_container);
            holder.absenceButton = (TextView) convertView.findViewById(R.id.absence_button);
            holder.lateButton = (TextView) convertView.findViewById(R.id.late_button);

            //Update student details text size
            holder.studentName.setTextSize(studentNameTextSize);
            holder.studentCourse.setTextSize(studentInfoTextHeight);

            convertView.setTag(holder);
        }
        else
            holder = (ViewHolder) convertView.getTag();



        //Get student details
        final StudentData studentData = tweets.get(position);

        //Holder clone
        final ViewHolder holderClone = holder;

        //Update V icon params
        FrameLayout.LayoutParams vIconParams = new FrameLayout.LayoutParams(vIconSize, vIconSize);

        vIconParams.gravity = Gravity.CENTER | Gravity.RIGHT;
        vIconParams.setMargins(0, 0, (int)(rowWidth * 0.04), 0);

        holderClone.vIcon.setLayoutParams(vIconParams);

        //Container onClick
        holderClone.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("Yoni", "Container click");

                int studentStatus = 0;

                try{
                    studentStatus = studentsStatus.get(studentData.STUDENTID);
                }
                catch (Exception e){}

                if(studentStatus < 1){
                    holderClone.vIcon.setVisibility(View.GONE);
                    holderClone.buttonsContainer.setVisibility(View.VISIBLE);

                    setLate(holderClone, studentData);
                }
                else{
                    holderClone.vIcon.setVisibility(View.VISIBLE);
                    holderClone.buttonsContainer.setVisibility(View.GONE);

                    studentsStatus.put(studentData.STUDENTID, 0);
                }

            }
        });

        //Minus button onClick
        holderClone.absenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setMinus(holderClone, studentData);
            }
        });

        //Late button onClick
        holderClone.lateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLate(holderClone, studentData);
            }
        });


        if(studentData.IMAGE != null)
        {
            Log.e("yoni", "position: " + position + ", " + holder.studentImage.getId() + ", " + studentData.STUDENTNAME);
            holder.studentImage.setImageBitmap(studentData.IMAGE);
        }

        holderClone.studentName.setText(studentData.STUDENTNAME);
        holderClone.studentCourse.setText(studentData.STUDENTINFO);

        convertView.setLayoutParams(new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, rowHeight));

        return convertView;

    }

    private void setLate(ViewHolder holder, StudentData studentData){
        setButtonStyle(holder.absenceButton, false);
        setButtonStyle(holder.lateButton, true);

        studentsStatus.put(studentData.STUDENTID, 1);
    }

    private void setMinus(ViewHolder holder, StudentData studentData){
        setButtonStyle(holder.absenceButton, true);
        setButtonStyle(holder.lateButton, false);

        studentsStatus.put(studentData.STUDENTID, 1);
    }

    private void setButtonStyle(TextView button, boolean selected){

        GradientDrawable shapeDrawable = (GradientDrawable) button.getBackground();

        if(selected){
            shapeDrawable.setColor(activity.getResources().getColor(R.color.select_menu_border));
            button.setTextColor(Color.WHITE);
        }
        else
        {
            shapeDrawable.setColor(Color.WHITE);
            button.setTextColor(activity.getResources().getColor(R.color.select_menu_border));
        }

    }

    public String getPropertiesString(String key) {
        if (mApp.mPropertiesSpecific.containsKey(key)) {
            return (String) mApp.mPropertiesSpecific.get(key);
        }
        else {
            return null;
        }
    }

    public float pixelsToSp(float px) {
        float scaledDensity = activity.getResources().getDisplayMetrics().scaledDensity;
        return px/scaledDensity;
    }

}